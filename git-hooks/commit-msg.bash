#!/bin/sh

echo "Analyse commit message ..."

red='\033[0;31m'
no_color='\033[0m'

INPUT_FILE=$1
START_LINE=`head -n1 $INPUT_FILE`

PATTERN="^(Bueno)-[[:digit:]]+: "
if ! [[ "$START_LINE" =~ $PATTERN ]]; then
 echo -e >&2 ${red}"Commit was not allowed by commit-msg hook! Commit message outside project standard. See an example, Bueno-XXX: Commit message"${no_color}
 exit 1
fi

echo "Commit was allowed by commit-msg hook!"
exit 0