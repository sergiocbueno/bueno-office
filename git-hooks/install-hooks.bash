#!/usr/bin/env bash

# If any command inside script returns error, exit and return that error 
set -e

blue='\033[0;34m'
green='\033[0;32m'
yellow='\033[0;33m'
no_color='\033[0m'

GIT_DIR=$(git rev-parse --git-dir)

echo -e ${blue}
echo "======================================"
echo "=          Setting up hooks!         ="
echo "======================================"

echo -e ${yellow}
echo "Installing commit-msg hook ..."
cp $GIT_DIR/../git-hooks/commit-msg.bash $GIT_DIR/hooks/commit-msg
echo -e ${green}
echo "Commit-msg hook installed sucessfully!"

echo -e ${yellow}
echo "Installing pre-commit hook ..."
cp $GIT_DIR/../git-hooks/pre-commit.bash $GIT_DIR/hooks/pre-commit
echo -e ${green}
echo "Pre-commit hook installed sucessfully!"

echo "All hooks installed sucessfully!"
echo -e ${no_color}