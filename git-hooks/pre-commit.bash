#!/usr/bin/env bash

echo "Analyzing your commit ..."
./git-hooks/analyze-commit.bash

# $? stores exit value of the last command
if [ $? -ne 0 ]; then
 echo "Commit was canceled by pre-commit hook!"
 exit 1
else
 echo "Commit was allowed by pre-commit hook!"
fi
