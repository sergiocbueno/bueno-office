#!/usr/bin/env bash

# magic line to ensure that we're always inside the root of our application,
# no matter from which directory we'll run script
# thanks to it we can just enter `./git-hooks/analyze-commit.bash`
cd "${0%/*}/.."

# Global variables
green='\033[0;32m'
red='\033[0;31m'
no_color='\033[0m'
git_dir=$(git rev-parse --git-dir)

# Build section
echo "Build solution ..."

solution_builder="C:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe"
solution=$git_dir/../BuenoOffice.sln

build_result=`"$solution_builder" "$solution"`
if [ $? == 0 ]; then
	echo -e >&2 ${green}"Build successfully!"${no_color}
else
    echo -e >&2 ${red}"Build failed!"${no_color}
	exit 1
fi

# Unit tests section
echo "Running all unit test ..."
echo -e ${red}

test_runner="C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\IDE\CommonExtensions\Microsoft\TestWindow\vstest.console.exe"
tests_dll=$git_dir/../BuenoOffice.Tests/bin/Debug/BuenoOffice.Tests.dll
tests_category="/TestCaseFilter:TestCategory=UnitTest"

tests_result=`"$test_runner" "$tests_dll" "$tests_category"`
if [ $? == 0 ]; then
	echo -e >&2 ${green}"All tests run successfully!"${no_color}
	exit 0
fi
echo -e >&2 ${no_color}
exit 1
