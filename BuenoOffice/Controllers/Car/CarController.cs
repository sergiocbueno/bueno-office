﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BuenoOffice.DependecyInjection;
using BuenoOffice.Mapping;
using BuenoOffice.Service.DTOs.Car;
using BuenoOffice.Service.Services.Car.Interface;
using BuenoOffice.ViewModels.Car;
using Microsoft.Practices.Unity;

namespace BuenoOffice.Controllers.Car
{
    public class CarController : Controller
    {
        private readonly IUnityContainer container;

        public CarController()
        {
            container = ContainerConfiguration.Container;
        }

        [Authorize]
        public ActionResult Index()
        {
            var carService = container.Resolve<ICarService>();
            var mapper = ServiceMapper.ConfigureMapper();

            var myCarsViewModel = new MyCarsViewModel();
            myCarsViewModel.NewCar = new CreateCarViewModel();
            InitializeDropDown(myCarsViewModel, null, null);

            var email = System.Web.HttpContext.Current.User.Identity.Name;
            var cars = carService.GetUserCarsByUserMail(email);
            myCarsViewModel.Cars = cars != null && !cars.Count.Equals(0) ? mapper.Map<List<CarViewModel>>(cars) : new List<CarViewModel>();

            return View(myCarsViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCar(MyCarsViewModel carsInfo)
        {
            var carService = container.Resolve<ICarService>();
            var mapper = ServiceMapper.ConfigureMapper();
            const string index = "Index";

            InitializeDropDown(carsInfo, carsInfo.NewCar.SelectBrandId, carsInfo.NewCar.SelectModelId);
            var email = System.Web.HttpContext.Current.User.Identity.Name;

            if (ModelState.IsValid)
            {
                var newCar = mapper.Map<NewCarDto>(carsInfo.NewCar);
                carService.CreateCar(newCar, email);
                ViewBag.Status = true;
                return RedirectToAction(index);
            }

            var cars = carService.GetUserCarsByUserMail(email);
            carsInfo.Cars = cars != null && !cars.Count.Equals(0) ? mapper.Map<List<CarViewModel>>(cars) : new List<CarViewModel>();

            return View(index, carsInfo);
        }

        [HttpPost]
        public JsonResult GetAllModelsByBrand(int brandId)
        {
            var carService = container.Resolve<ICarService>();
            var models = carService.GetAllModelsByBrand(brandId);

            if (models == null || !models.Any())
            {
                return Json(new { success = false, responseText = $"Any model was found for this brand id = {brandId}"}, JsonRequestBehavior.AllowGet);
            }
            
            var result = new List<object>();
            foreach (var model in models)
            {
                result.Add(new { model.Text, model.Value, model.Selected });
            }

            return Json(new { success = true, data = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetAllVersionsByModel(int modelId)
        {
            var carService = container.Resolve<ICarService>();
            var versions = carService.GetAllVersionsByModel(modelId);

            if (versions == null || !versions.Any())
            {
                return Json(new { success = false, responseText = $"Any version was found for this model id = {modelId}" }, JsonRequestBehavior.AllowGet);
            }

            var result = new List<object>();
            foreach (var version in versions)
            {
                result.Add(new { version.Text, version.Value, version.Selected });
            }

            return Json(new { success = true, data = result }, JsonRequestBehavior.AllowGet);
        }

        #region Private Methods

        private void InitializeDropDown (MyCarsViewModel myCars, int? brandId, int? modelId)
        {
            var carService = container.Resolve<ICarService>();

            myCars.NewCar.Brands = carService.GetAllBrands();
            myCars.NewCar.Models = brandId != null ? carService.GetAllModelsByBrand(brandId.Value) : new List<SelectListItem>();
            myCars.NewCar.Versions = modelId != null ? carService.GetAllVersionsByModel(modelId.Value) : new List<SelectListItem>();
            myCars.NewCar.Conversions = carService.GetAllConversions();
            myCars.NewCar.Colors = carService.GetAllColors();
        }

        #endregion
    }
}