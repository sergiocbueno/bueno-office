﻿using System.Web.Mvc;

namespace BuenoOffice.Controllers.Error
{
    public class ErrorController : Controller
    {
        public ActionResult ServerError()
        {
            ViewBag.Message = "Algum erro ocorreu no servidor, tente mais tarde!";
            return View("Error");
        }

        public ActionResult NotFound()
        {
            ViewBag.Message = "Não foi possível localizar a página!";
            return View("Error");
        }
    }
}