﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using BuenoOffice.DependecyInjection;
using BuenoOffice.Mapping;
using BuenoOffice.Service.DTOs.User;
using BuenoOffice.Service.Helpers.User;
using BuenoOffice.Service.Services.User.Interface;
using BuenoOffice.ViewModels.User;
using Microsoft.Practices.Unity;

namespace BuenoOffice.Controllers.User
{
    public class UserController : Controller
    {
        private readonly IUnityContainer container;

        public UserController()
        {
            container = ContainerConfiguration.Container;
        }

        public ActionResult Index()
        {
            var message = TempData["ForgottenResetPasswordSucess"]?.ToString();
            if (!string.IsNullOrEmpty(message))
            {
                ViewBag.Status = true;
                ViewBag.Message = message;
            }

            return View();
        }

        public ActionResult Registration()
        {
            return View();
        }

        public ActionResult ForgottenPassword()
        {
            return View();
        }

        public ActionResult Employee()
        {
            return View();
        }

        public ActionResult TermsAndConditions()
        {
            return View();
        }

        [Authorize]
        public ActionResult EmployeeManagement()
        {
            var employeeManagement = new EmployeeManagementViewModel();
            employeeManagement.EmployeeName = string.Empty;
            employeeManagement.PageNumber = 1;
            employeeManagement.PageSize = 10;

            return View(employeeManagement);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SearchEmployeeByName(EmployeeManagementViewModel employeeManagement)
        {
            const string manager = "EmployeeManagement";

            if (ModelState.IsValid)
            {
                var userService = container.Resolve<IUserService>();
                var mapper = ServiceMapper.ConfigureMapper();
                var currentSearch = employeeManagement.EmployeeName;

                var pagedList = userService.GetPagedEmployeeUserByName(currentSearch, employeeManagement.PageNumber, employeeManagement.PageSize);
                employeeManagement = mapper.Map<EmployeeManagementViewModel>(pagedList);
                employeeManagement.EmployeeName = currentSearch;

                return View(manager, employeeManagement);
            }

            return View(manager, employeeManagement);
        }

        [Authorize]
        public ActionResult GetEmployeeManagerByPage(string currentSearch, int pageNumber, int pageSize)
        {
            var viewModel = new EmployeeManagementViewModel
            {
                EmployeeName = currentSearch,
                PageSize = pageSize,
                PageNumber = pageNumber
            };

            return SearchEmployeeByName(viewModel);
        }

        [Authorize]
        [HttpPost]
        public ActionResult RemoveEmployee(int employeeId, string currentSearch, int pageNumber, int pageSize)
        {
            const string getEmployeeManagerByPage = "GetEmployeeManagerByPage";
            var userService = container.Resolve<IUserService>();

            userService.RemoveEmployeeById(employeeId);

            return RedirectToAction(getEmployeeManagerByPage, new { currentSearch, pageNumber, pageSize });
        }

        [Authorize]
        public ActionResult EditUser()
        {
            var userService = container.Resolve<IUserService>();
            var mapper = ServiceMapper.ConfigureMapper();

            var email = System.Web.HttpContext.Current.User.Identity.Name;
            var userDto = userService.GetUserByEmail(email);
            var userEditViewModel = mapper.Map<UserEditViewModel>(userDto);
            return View("Edit", userEditViewModel);
        }

        [Authorize]
        public ActionResult ResetPassword()
        {
            var message = TempData["ResetPasswordSucess"]?.ToString();
            if (!string.IsNullOrEmpty(message))
            {
                ViewBag.Status = true;
                ViewBag.Message = message;
            }
            var email = System.Web.HttpContext.Current.User.Identity.Name;
            var userResetPasswordViewModel = new UserResetPasswordViewModel { Email = email };
            return View("ResetPassword", userResetPasswordViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterUser(UserRegistrationViewModel user)
        {
            const string registration = "Registration";
            const string validate = "Validate";

            if (ModelState.IsValid && user.TermsAndConditions)
            {
                var userService = container.Resolve<IUserService>();
                var mapper = ServiceMapper.ConfigureMapper();

                user.Image = ConvertHttpPostedFileToBytes(System.Web.HttpContext.Current.Request.Files[0]);
                var userDto = mapper.Map<UserDto>(user);
                userService.CreateUser(userDto);
                ViewBag.Status = true;
                return View(validate);
            }

            return View(registration, user);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterEmployee(EmployeeRegistrationViewModel employee)
        {
            const string employeeView = "Employee";

            if (ModelState.IsValid)
            {
                var userService = container.Resolve<IUserService>();
                userService.CreateEmployee(employee.EmployeeMail);
                ViewBag.Status = true;
                ViewBag.Message = "Empregado criado com sucesso! Primeiro login: " + employee.EmployeeMail + " e senha: mecanicabueno";
                return View(employeeView);
            }

            return View(employeeView, employee);
        }

        [HttpGet]
        public ActionResult VerifyAccount(string id)
        {
            const string invalid = "Link inválido!";
            const string valid = "Ativação de conta realiza com sucesso!";
            const string activation = "Activation";

            var userService = container.Resolve<IUserService>();
            var isValid = userService.VerifyAccount(id);
            ViewBag.Status = isValid;
            ViewBag.Message = isValid ? valid : invalid;

            return View(activation);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LoginUser(UserLoginViewModel user)
        {
            const string index = "Index";
            const string home = "Home";
            const string error = "Email ou senha inválida!";

            if (ModelState.IsValid)
            {
                var userService = container.Resolve<IUserService>();
                var userContext = userService.LoginActivedUser(user.Email, user.Password);

                if (userContext != null)
                {
                    Authentication(userContext, user.RememberMe);
                    return RedirectToAction(index, home);
                }

                ViewBag.Status = false;
                ViewBag.Message = error;
                return View(index, user);
            }

            return View(index, user);
        }

        [Authorize]
        public ActionResult Logout()
        {
            const string index = "Index";
            FormsAuthentication.SignOut();
            return RedirectToAction(index);
        }

        [HttpPost]
        public JsonResult HasEmailAlreadyExist(string email)
        {
            var userService = container.Resolve<IUserService>();
            var isValid = userService.IsValidMail(email);
            return Json(new { success = isValid }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RequestNewPassword(UserBaseEmail user)
        {
            const string text = "Um e-mail com as instruções de como mudar sua senha foi enviado para: ";
            const string forgottenPassword = "ForgottenPassword";

            var userService = container.Resolve<IUserService>();
            var isValid = userService.RequestNewPassword(user.Email);
            ViewBag.Status = isValid;
            ViewBag.Message = text + user.Email;

            return View(forgottenPassword, user);
        }

        [HttpGet]
        public ActionResult VerifyNewPassword(string id)
        {
            const string invalid = "InvalidLink";
            const string reset = "ForgottenResetPassword";

            var userService = container.Resolve<IUserService>();
            var isValid = userService.VerifyNewPassword(id);

            if (isValid)
            {
                var viewModel = new UserForgottenResetPasswordViewModel { Guid = id };
                return View(reset, viewModel);
            }

            return View(invalid);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgottenResetPassword(UserForgottenResetPasswordViewModel setting)
        {
            const string error = "Ocorreu um emprevisto inesperado, tente novamente mais tarde!";
            const string sucess = "Senha alterada com sucesso!";
            const string forgottenResetPassword = "ForgottenResetPassword";

            if (ModelState.IsValid)
            {
                var userService = container.Resolve<IUserService>();
                var mapper = ServiceMapper.ConfigureMapper();

                var forgottenResetPasswordDto = mapper.Map<UserForgottenResetPasswordDto>(setting);
                var isValid = userService.ForgottenResetPassword(forgottenResetPasswordDto);

                if (isValid)
                {
                    TempData["ForgottenResetPasswordSucess"] = sucess;
                    return RedirectToAction("Index");
                }

                ViewBag.Status = false;
                ViewBag.Message = error;
                return View(forgottenResetPassword, setting);
            }

            return View(forgottenResetPassword, setting);
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(UserResetPasswordViewModel setting)
        {
            const string error = "Senha atual incorreta!";
            const string sucess = "Senha alterada com sucesso!";
            const string resetPassword = "ResetPassword";

            if (ModelState.IsValid)
            {
                var userService = container.Resolve<IUserService>();
                var mapper = ServiceMapper.ConfigureMapper();

                var resetPasswordDto = mapper.Map<UserResetPasswordDto>(setting);
                var isValid = userService.ResetPassword(resetPasswordDto);

                if (isValid)
                {
                    TempData["ResetPasswordSucess"] = sucess;
                    return RedirectToAction(resetPassword);
                }

                ViewBag.Status = false;
                ViewBag.Message = error;
                return View(resetPassword, setting);
            }

            return View(resetPassword, setting);
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult EditUser(UserEditViewModel userInfo)
        {
            const string sucess = "Alteração realizada com sucesso!";
            const string editUser = "Edit";

            if (ModelState.IsValid)
            {
                var userService = container.Resolve<IUserService>();
                var mapper = ServiceMapper.ConfigureMapper();
                var uploadImage = ConvertHttpPostedFileToBytes(System.Web.HttpContext.Current.Request.Files[0]);

                if (userInfo.Image != null && userInfo.Image.Length > 0 && uploadImage != null && uploadImage.Length > 0)
                {
                    userInfo.Image = uploadImage;
                }
                else if (uploadImage != null && uploadImage.Length > 0)
                {
                    userInfo.Image = uploadImage;
                }

                var userDto = mapper.Map<UserDto>(userInfo);
                userService.UpdateUser(userDto);
                ViewBag.Status = true;
                ViewBag.Message = sucess;

                return View(editUser, userInfo);
            }

            return View(editUser, userInfo);
        }

        private void Authentication(UserContext userContext, bool rememberMe)
        {
            var nextYear = DateTime.Now.AddYears(1);
            var serializer = new JavaScriptSerializer();
            var userRoleSerialized = serializer.Serialize(userContext.Role);

            var ticket = new FormsAuthenticationTicket(1, userContext.Email, DateTime.Now, nextYear, rememberMe, userRoleSerialized);
            var encrypted = FormsAuthentication.Encrypt(ticket);
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
            cookie.Expires = nextYear;
            cookie.HttpOnly = true;
            Response.Cookies.Add(cookie);
        }

        private byte[] ConvertHttpPostedFileToBytes(HttpPostedFile file)
        {
            var memoryStream = new MemoryStream();
            file.InputStream.CopyTo(memoryStream);
            return memoryStream.ToArray();
        }
    }
}