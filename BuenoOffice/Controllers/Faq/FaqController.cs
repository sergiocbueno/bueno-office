﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using BuenoOffice.DependecyInjection;
using BuenoOffice.Infrastructure.Utilities.Enums.Faq;
using BuenoOffice.Mapping;
using BuenoOffice.Service.DTOs.Faq;
using BuenoOffice.Service.Services.Faq.Interface;
using BuenoOffice.ViewModels.Faq;
using Microsoft.Practices.Unity;

namespace BuenoOffice.Controllers.Faq
{
    public class FaqController : Controller
    {
        private readonly IUnityContainer container;

        public FaqController()
        {
            container = ContainerConfiguration.Container;
        }

        public ActionResult Index()
        {
            var faqService = container.Resolve<IFaqService>();
            var mapper = ServiceMapper.ConfigureMapper();

            var faqDtos = faqService.LastQuestions();
            var faqViewModel = new FaqViewModel();
            faqViewModel.Answers = mapper.Map<List<FaqAnswerViewModel>>(faqDtos);

            return View(faqViewModel);
        }

        [Authorize]
        public ActionResult FaqManager()
        {
            var faqService = container.Resolve<IFaqService>();

            var faqManagerViewModel = new FaqManagerViewModel();
            faqManagerViewModel.FaqStatus = faqService.GetAllFaqStatus();
            faqManagerViewModel.Search = string.Empty;
            faqManagerViewModel.PageNumber = 1;
            faqManagerViewModel.PageSize = 10;

            return View(faqManagerViewModel);
        }

        [HttpPost]
        public ActionResult SearchQuestion(string search)
        {
            const string indexFaq = "Index";
            var faqService = container.Resolve<IFaqService>();
            var mapper = ServiceMapper.ConfigureMapper();

            if (!string.IsNullOrEmpty(search))
            {
                var faqDtos = faqService.SearchQuestions(search);
                var faqViewModel = new FaqViewModel();
                faqViewModel.Answers = mapper.Map<List<FaqAnswerViewModel>>(faqDtos);

                return View(indexFaq, faqViewModel);
            }

            return RedirectToAction(indexFaq);
        }

        [HttpPost]
        public ActionResult SendQuestion(string question)
        {
            const string indexFaq = "Index";
            var faqService = container.Resolve<IFaqService>();

            if (!string.IsNullOrEmpty(question))
            {
                var faqDto = new FaqDto { Question = question };
                faqService.CreateOrUpdate(faqDto);
            }

            return RedirectToAction(indexFaq);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SearchFaqByQuestionAndStatusAndPage(FaqManagerViewModel faqManager)
        {
            const string manager = "FaqManager";

            if (ModelState.IsValid)
            {
                var faqService = container.Resolve<IFaqService>();
                var mapper = ServiceMapper.ConfigureMapper();
                var previousSearch = faqManager.Search;
                var previousFaqStatusId = faqManager.SelectFaqStatusId;

                var pagedList = faqService.GetPagedFaqByQuestionAndStatus(faqManager.Search, (FaqStatus)faqManager.SelectFaqStatusId, faqManager.PageNumber, faqManager.PageSize);
                faqManager = mapper.Map<FaqManagerViewModel>(pagedList);
                faqManager.Search = previousSearch;
                faqManager.SelectFaqStatusId = previousFaqStatusId;
                faqManager.FaqStatus = faqService.GetAllFaqStatus();

                return View(manager, faqManager);
            }

            return View(manager, faqManager);
        }

        [Authorize]
        public ActionResult GetFaqManagerByPage(string search, int faqStatusId, int pageNumber, int pageSize)
        {
            var viewModel = new FaqManagerViewModel
            {
                Search = search,
                SelectFaqStatusId = faqStatusId,
                PageSize = pageSize,
                PageNumber = pageNumber
            };

            return SearchFaqByQuestionAndStatusAndPage(viewModel);
        }

        [Authorize]
        [HttpPost]
        public ActionResult FaqManagerDetails(int faqId)
        {
            const string faqManagerModal = "FaqManagerModal";
            var faqService = container.Resolve<IFaqService>();
            var mapper = ServiceMapper.ConfigureMapper();

            var faqDto = faqService.GetFaqById(faqId);
            var faq = mapper.Map<FaqAnswerViewModel>(faqDto);
            return PartialView(faqManagerModal, faq);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CompleteFaq(FaqAnswerViewModel faq)
        {
            const string faqManager = "FaqManager";

            if (ModelState.IsValid)
            {
                var faqService = container.Resolve<IFaqService>();
                var mapper = ServiceMapper.ConfigureMapper();

                var faqDto = mapper.Map<FaqDto>(faq);
                faqDto.AnswerDate = DateTime.Now;
                faqService.CreateOrUpdate(faqDto);

                return RedirectToAction(faqManager);
            }

            return RedirectToAction(faqManager);
        }

        [Authorize]
        [HttpPost]
        public ActionResult DisableFaq(int faqId, string search, int faqStatusId, int pageNumber, int pageSize)
        {
            const string getFaqManagerByPage = "GetFaqManagerByPage";
            var faqService = container.Resolve<IFaqService>();

            faqService.DisableFaq(faqId);

            return RedirectToAction(getFaqManagerByPage, new { search, faqStatusId, pageNumber, pageSize });
        }
    }
}