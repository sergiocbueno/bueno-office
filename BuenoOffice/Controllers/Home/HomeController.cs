﻿using System.Web.Mvc;

namespace BuenoOffice.Controllers.Home
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}