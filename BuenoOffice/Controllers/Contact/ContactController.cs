﻿using System.Web.Mvc;
using BuenoOffice.DependecyInjection;
using BuenoOffice.Mapping;
using BuenoOffice.Service.DTOs.Contact;
using BuenoOffice.Service.Services.Contact.Interface;
using BuenoOffice.ViewModels.Contact;
using Microsoft.Practices.Unity;

namespace BuenoOffice.Controllers.Contact
{
    public class ContactController : Controller
    {
        private readonly IUnityContainer container;

        public ContactController()
        {
            container = ContainerConfiguration.Container;
        }

        public ActionResult Index()
        {
            var message = TempData["SendMessageSucess"]?.ToString();
            if (!string.IsNullOrEmpty(message))
            {
                ViewBag.Status = true;
                ViewBag.Message = message;
            }
            return View();
        }

        [HttpPost]
        public ActionResult SendMessage(ContactMessageViewModel contactMessage)
        {
            const string indexContact = "Index";
            var contactService = container.Resolve<IContactService>();
            var mapper = ServiceMapper.ConfigureMapper();

            if (ModelState.IsValid)
            {
                var contactMessageDto = mapper.Map<ContactMessageDto>(contactMessage);
                contactService.SendMessage(contactMessageDto);

                TempData["SendMessageSucess"] = "Mensagem enviada com sucesso.";
                return RedirectToAction(indexContact);
            }

            ViewBag.Status = false;
            ViewBag.Message = "Preencha os campos obrigatórios corretamente.";
            return View(indexContact, contactMessage);
        }

        [HttpPost]
        public JsonResult IsValidEmail(string email)
        {
            var contactService = container.Resolve<IContactService>();
            var isValid = contactService.IsValidEmail(email);
            return Json(new { success = isValid }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult IsValidPhone(string phone)
        {
            var contactService = container.Resolve<IContactService>();
            var isValid = contactService.IsValidPhone(phone);
            return Json(new { success = isValid }, JsonRequestBehavior.AllowGet);
        }
    }
}