﻿using AutoMapper;
using BuenoOffice.Mapping.Profiles.Car;
using BuenoOffice.Mapping.Profiles.Contact;
using BuenoOffice.Mapping.Profiles.Faq;
using BuenoOffice.Mapping.Profiles.User;

namespace BuenoOffice.Mapping
{
    public static class ServiceMapper
    {
        private static bool _isMapped;
        private static IMapper _mapper;

        public static IMapper ConfigureMapper()
        {
            if (!_isMapped)
            {
                _mapper = new MapperConfiguration(map =>
                {
                    map.AddProfile<ContactMappingProfile>();
                    map.AddProfile<UserMappingProfile>();
                    map.AddProfile<CarMappingProfile>();
                    map.AddProfile<FaqMappingProfile>();
                }).CreateMapper();


                _isMapped = true;
            }

            return _mapper;
        }
    }
}