﻿using AutoMapper;
using BuenoOffice.Service.DTOs.Contact;
using BuenoOffice.ViewModels.Contact;

namespace BuenoOffice.Mapping.Profiles.Contact
{
    public class ContactMappingProfile : Profile
    {
        public ContactMappingProfile()
        {
            CreateMap<ContactMessageViewModel, ContactMessageDto>().ReverseMap();
        }
    }
}