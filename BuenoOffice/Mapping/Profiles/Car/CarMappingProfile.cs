﻿using AutoMapper;
using BuenoOffice.Service.DTOs.Car;
using BuenoOffice.ViewModels.Car;

namespace BuenoOffice.Mapping.Profiles.Car
{
    public class CarMappingProfile : Profile
    {
        public CarMappingProfile()
        {
            CreateMap<CarDto, CarViewModel>()
                .ForMember(dest => dest.Conversion, opt => opt.MapFrom(src => src.Conversion.Name))
                .ForMember(dest => dest.Color, opt => opt.MapFrom(src => src.Color.Name))
                .ForMember(dest => dest.Version, opt => opt.MapFrom(src => src.Version.Name))
                .ForMember(dest => dest.Mileage, opt => opt.MapFrom(src => src.Mileage))
                .ForMember(dest => dest.ModelYear, opt => opt.MapFrom(src => src.ModelYear))
                .ForMember(dest => dest.ManufactureYear, opt => opt.MapFrom(src => src.ManufactureYear));
            CreateMap<CreateCarViewModel, NewCarDto>();
        }
    }
}