﻿using AutoMapper;
using BuenoOffice.Service.DTOs.Faq;
using BuenoOffice.Service.Helpers.Paged;
using BuenoOffice.ViewModels.Faq;

namespace BuenoOffice.Mapping.Profiles.Faq
{
    public class FaqMappingProfile : Profile
    {
        public FaqMappingProfile()
        {
            CreateMap<FaqDto, FaqAnswerViewModel>().ReverseMap();
            CreateMap<PagedList<FaqDto>, FaqManagerViewModel>();
        }
    }
}