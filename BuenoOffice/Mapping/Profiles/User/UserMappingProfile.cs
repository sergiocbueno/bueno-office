﻿using AutoMapper;
using BuenoOffice.Service.DTOs.User;
using BuenoOffice.Service.Helpers.Paged;
using BuenoOffice.ViewModels.User;

namespace BuenoOffice.Mapping.Profiles.User
{
    public class UserMappingProfile : Profile
    {
        public UserMappingProfile()
        {
            CreateMap<UserRegistrationViewModel, UserDto>();
            CreateMap<UserLoginViewModel, UserDto>();
            CreateMap<UserForgottenResetPasswordViewModel, UserForgottenResetPasswordDto>();
            CreateMap<UserResetPasswordViewModel, UserResetPasswordDto>();
            CreateMap<UserEditViewModel, UserDto>().ReverseMap();
            CreateMap<UserDto, EmployeeDetailsViewModel>();
            CreateMap<PagedList<UserDto>, EmployeeManagementViewModel>();
        }
    }
}