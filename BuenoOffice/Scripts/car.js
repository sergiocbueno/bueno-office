﻿window.onload = function () {
        document.getElementById('brandDropDown').onchange = function () {
        var brandId = $('#brandDropDown').val();
        updateModelDropDown(brandId);
        }
        document.getElementById('modelDropDown').onchange = function () {
            var modelId = $('#modelDropDown').val();
            updateVersionDropDown(modelId);
        }
}

function updateModelDropDown(brandId) {
    $.ajax({
        type: "POST",
        data: { brandId: brandId },
        url: '/Car/GetAllModelsByBrand',
        success: function (response) {
            if (response != null && response.success) {
                $("#modelDropDown").empty();
                $("#modelDropDown").append("<option value>Escolha o modelo...</option>");
                $.each(response.data, function (index, info) {
                    $("#modelDropDown").append("<option value='" + info.Value + "'>" + info.Text + "</option>");
                });
                $("#versionDropDown").empty();
                $("#versionDropDown").append("<option value>Escolha a versão...</option>");
            } else {
                $("#modelDropDown").empty();
                $("#versionDropDown").empty();
            }
        }
    });
}

function updateVersionDropDown(modelId) {
    $.ajax({
        type: "POST",
        data: { modelId: modelId },
        url: '/Car/GetAllVersionsByModel',
        success: function (response) {
            if (response != null && response.success) {
                $("#versionDropDown").empty();
                $("#versionDropDown").append("<option value>Escolha a versão...</option>");
                $.each(response.data, function (index, info) {
                    $("#versionDropDown").append("<option value='" + info.Value + "'>" + info.Text + "</option>");
                });
            } else {
                $("#versionDropDown").empty();
            }
        }
    });
}