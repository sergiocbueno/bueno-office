﻿window.onload = function () {
    var rows = document.getElementsByTagName("tr");
    for (var index = 1; index < rows.length; index++) {
        rows[index].onclick = function () {
            rowAction(this);
        };
    }
}

function rowAction(row) {
    var id = row.id;
    $.ajax({
        type: "POST",
        url: '/Faq/FaqManagerDetails',
        data: { faqId: id },
        success: function (response) {
            $('#myModalContent').html(response);
            $('#myModal').modal('show');

            document.getElementById('disabledMod').onclick = function () {
                var idToDisable = $('#disabledMod').val();
                var search = $('#search').val();
                var faqStatus = $('#faqStatusDropDown').val();
                var pageNumber = $('#PageNumber').val();
                var pageSize = $('#PageSize').val();
                disableAction(idToDisable, search, faqStatus, pageNumber, pageSize);
            }
        },
        error: function () {
            alert("An error occured during open modal window process");
        }
    });
}

function disableAction(id, search, faqStatusId, pageNumber, pageSize) {
    $.ajax({
        type: "POST",
        url: '/Faq/DisableFaq',
        data: { faqId: id, search: search, faqStatusId: faqStatusId, pageNumber: pageNumber, pageSize: pageSize },
        success: function () {
            location.reload();
        },
        error: function () {
            alert("An error occured during disable faq process");
        }
    });
}