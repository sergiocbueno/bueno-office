﻿window.onload = function () {
    document.getElementById("employeeCreator").disabled = true;

    document.getElementById('employeeNameText').onchange = function () {
        var email = $('#employeeNameText').val();
        hasEmailAlreadyExist(email);
    }
}

function hasEmailAlreadyExist(email) {
    $.ajax({
        type: "POST",
        data: { email: email },
        url: '/User/HasEmailAlreadyExist',
        success: function (response) {
            if (response != null && response.success) {
                document.getElementById("employeeCreator").disabled = false;
            } else {
                document.getElementById("employeeCreator").disabled = true;
            }
        }
    });
}