﻿window.onload = function () {
        enabledButton();

        document.getElementById('TermsAndConditions').onchange = function () {
            enabledButton();
        }

        document.getElementById('emailText').onchange = function () {
            var email = $('#emailText').val();
            hasEmailAlreadyExist(email);
        }
}

function hasEmailAlreadyExist(email) {
    $.ajax({
        type: "POST",
        data: { email: email },
        url: '/User/HasEmailAlreadyExist',
        success: function (response) {
            if (response != null && response.success) {
                document.getElementById("emailText").style.border = "1px solid green";
                var isChecked = document.querySelector('#TermsAndConditions').checked;
                if (isChecked) {
                    document.getElementById("createButton").disabled = false;
                }
            } else {
                document.getElementById("emailText").style.border = "1px solid red";
                document.getElementById("createButton").disabled = true;
            }
        }
    });
}

function enabledButton() {
    var isChecked = document.querySelector('#TermsAndConditions').checked;

    if (isChecked) {
        document.getElementById("createButton").disabled = false;
    } else {
        document.getElementById("createButton").disabled = true;
    }
}