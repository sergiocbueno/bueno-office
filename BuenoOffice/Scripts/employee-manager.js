﻿window.onload = function () {
    var buttons = document.getElementsByTagName("button");
    for (var index = 1; index < buttons.length; index++) {
        if (buttons[index].type == "reset") {
            buttons[index].onclick = function () {
                buttonAction(this);
            };
        }
    }
}

function buttonAction(button) {
    var id = button.id;
    var search = $('#employee-search').val();
    var pageNumber = $('#PageNumber').val();
    var pageSize = $('#PageSize').val();

    $.ajax({
        type: "POST",
        url: '/User/RemoveEmployee',
        data: { employeeId: id, currentSearch: search, pageNumber: pageNumber, pageSize: pageSize },
        success: function () {
            location.reload();
        },
        error: function () {
            alert("An error occurred during remove employee process");
        }
    });
}