﻿using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using BuenoOffice.DependecyInjection;

namespace BuenoOffice
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            ContainerConfiguration.ConfigureUnityContainer();
            log4net.Config.XmlConfigurator.Configure(new FileInfo(Server.MapPath("~/Web.config")));
        }
    }
}
