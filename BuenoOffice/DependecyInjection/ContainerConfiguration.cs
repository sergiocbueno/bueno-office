﻿using System.Data.Entity;
using System.Web.Mvc;
using BuenoOffice.Data.Entities.Brand.Interface;
using BuenoOffice.Data.Entities.Brand.Repository;
using BuenoOffice.Data.Entities.Car.Interface;
using BuenoOffice.Data.Entities.Car.Repository;
using BuenoOffice.Data.Entities.CarModel.Interface;
using BuenoOffice.Data.Entities.CarModel.Repository;
using BuenoOffice.Data.Entities.Color.Interface;
using BuenoOffice.Data.Entities.Color.Repository;
using BuenoOffice.Data.Entities.Conversion.Interface;
using BuenoOffice.Data.Entities.Conversion.Repository;
using BuenoOffice.Data.Entities.Faq.Interface;
using BuenoOffice.Data.Entities.Faq.Repository;
using BuenoOffice.Data.Entities.User.Interface;
using BuenoOffice.Data.Entities.User.Repository;
using BuenoOffice.Data.Entities.Version.Interface;
using BuenoOffice.Data.Entities.Version.Repository;
using BuenoOffice.Data.Parametrization.Context;
using BuenoOffice.Infrastructure.Utilities.Mail.Interface;
using BuenoOffice.Infrastructure.Utilities.Mail.Service;
using BuenoOffice.Service.Services.Car.Interface;
using BuenoOffice.Service.Services.Car.Service;
using BuenoOffice.Service.Services.Contact.Interface;
using BuenoOffice.Service.Services.Contact.Service;
using BuenoOffice.Service.Services.Faq.Interface;
using BuenoOffice.Service.Services.Faq.Service;
using BuenoOffice.Service.Services.User.Interface;
using BuenoOffice.Service.Services.User.Service;
using Microsoft.Practices.Unity;

namespace BuenoOffice.DependecyInjection
{
    public static class ContainerConfiguration
    {
        public static UnityContainer Container { get; set; }

        public static void ConfigureUnityContainer()
        {
            var container = new UnityContainer();
            RegisterServices(container);
            Container = container;
        }

        private static void RegisterServices(IUnityContainer container)
        {
            #region Parametrization Context

            container.RegisterType<IParametrizationContext, ParametrizationContext>(new HierarchicalLifetimeManager(), new InjectionConstructor());
            container.RegisterType<DbContext, ParametrizationContext>(new HierarchicalLifetimeManager(), new InjectionConstructor());

            #endregion

            #region Repository Context

            container.RegisterType<IUserRepository, UserRepository>();
            container.RegisterType<IFaqRepository, FaqRepository>();
            container.RegisterType<IBrandRepository, BrandRepository>();
            container.RegisterType<IModelRepository, ModelRepository>();
            container.RegisterType<IVersionRepository, VersionRepository>();
            container.RegisterType<IColorRepository, ColorRepository>();
            container.RegisterType<IConversionRepository, ConversionRepository>();
            container.RegisterType<ICarRepository, CarRepository>();

            #endregion

            #region Service Context

            container.RegisterType<ISmtpMail, SmtpMail>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IContactService, ContactService>();
            container.RegisterType<IFaqService, FaqService>();
            container.RegisterType<ICarService, CarService>();

            #endregion
        }
    }
}