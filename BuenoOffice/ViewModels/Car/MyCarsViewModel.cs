﻿using System.Collections.Generic;

namespace BuenoOffice.ViewModels.Car
{
    public class MyCarsViewModel
    {
        public List<CarViewModel> Cars { get; set; }
        public CreateCarViewModel NewCar { get; set; }
    }
}