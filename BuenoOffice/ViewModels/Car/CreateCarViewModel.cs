﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BuenoOffice.ViewModels.Car
{
    public class CreateCarViewModel
    {
        [Display(Name = "Ano do modelo: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Ano do modelo é necessário!")]
        public int? ModelYear { get; set; }

        [Display(Name = "Ano de fabricação: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Ano de fabricação é necessário!")]
        public int? ManufactureYear { get; set; }

        [Display(Name = "Quilometragem: ")]
        public int? Mileage { get; set; }

        [Display(Name = "Cor: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Cor é necessário!")]
        public int SelectColorId { get; set; }
        public IEnumerable<SelectListItem> Colors { get; set; }

        [Display(Name = "Câmbio: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Câmbio é necessário!")]
        public int SelectConversionId { get; set; }
        public IEnumerable<SelectListItem> Conversions { get; set; }

        [Display(Name = "Versão: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Versão é necessário!")]
        public int SelectVersionId { get; set; }
        public IEnumerable<SelectListItem> Versions { get; set; }

        [Display(Name = "Modelo: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Modelo é necessário!")]
        public int SelectModelId { get; set; }
        public IEnumerable<SelectListItem> Models { get; set; }

        [Display(Name = "Marca: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Marca é necessário!")]
        public int SelectBrandId { get; set; }
        public IEnumerable<SelectListItem> Brands { get; set; }
    }
}