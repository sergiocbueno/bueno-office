﻿namespace BuenoOffice.ViewModels.Car
{
    public class CarViewModel
    {
        public string Version { get; set; }
        public string Color { get; set; }
        public string Conversion { get; set; }
        public int ModelYear { get; set; }
        public int ManufactureYear { get; set; }
        public int? Mileage { get; set; }
    }
}