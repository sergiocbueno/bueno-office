﻿using System.ComponentModel.DataAnnotations;

namespace BuenoOffice.ViewModels.Contact
{
    public class ContactMessageViewModel
    {
        [Required(ErrorMessage = "Nome é necessário!")]
        [Display(Name = "Nome: *")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Email é necessário!")]
        [Display(Name = "Email: *")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Carro é necessário!")]
        [Display(Name = "Carro: *")]
        public string Car { get; set; }

        [Display(Name = "Telefone (com DDD):")]
        [DataType(DataType.PhoneNumber)]
        public int? Phone { get; set; }

        [Required(ErrorMessage = "Assunto é necessário!")]
        [Display(Name = "Assunto: *")]
        public string Subject { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Mensagem é necessário!")]
        [Display(Name = "Mensagem: *")]
        public string Message { get; set; }
    }
}