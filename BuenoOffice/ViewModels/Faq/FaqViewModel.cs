﻿using System.Collections.Generic;

namespace BuenoOffice.ViewModels.Faq
{
    public class FaqViewModel
    {
        public string Search { get; set; }
        public string Question { get; set; }
        public List<FaqAnswerViewModel> Answers { get; set; }
    }
}