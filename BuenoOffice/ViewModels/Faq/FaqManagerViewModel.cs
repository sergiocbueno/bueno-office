﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PagedList;

namespace BuenoOffice.ViewModels.Faq
{
    public class FaqManagerViewModel : IPagedList
    {
        [Display(Name = "Procurar: ")]
        public string Search { get; set; }

        [Display(Name = "Situação da pergunta: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Situação da pergunta é necessário!")]
        public int SelectFaqStatusId { get; set; }

        public IEnumerable<SelectListItem> FaqStatus { get; set; }

        public List<FaqAnswerViewModel> Data { get; set; }

        [Display(Name = "Paginação: ")]
        public int PageSize { get; set; }
        public int TotalItemCount { get; set; }
        public int PageNumber { get; set; }
        public int PageCount { get; set; }
        public bool HasPreviousPage { get; set; }
        public bool HasNextPage { get; set; }
        public bool IsFirstPage { get; set; }
        public bool IsLastPage { get; set; }
        public int FirstItemOnPage { get; set; }
        public int LastItemOnPage { get; set; }
    }
}