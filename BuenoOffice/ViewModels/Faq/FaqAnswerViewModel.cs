﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BuenoOffice.ViewModels.Faq
{
    public class FaqAnswerViewModel
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public DateTime QuestionDate { get; set; }
        [Required]
        public string Answer { get; set; }
        public DateTime? AnswerDate { get; set; }
    }
}