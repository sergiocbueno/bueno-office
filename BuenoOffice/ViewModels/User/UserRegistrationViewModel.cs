﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BuenoOffice.ViewModels.User
{
    public class UserRegistrationViewModel : UserBaseEmail
    {
        [Display(Name = "Nome: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Nome é necessário!")]
        public string FirstName { get; set; }

        [Display(Name = "Sobrenome: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Sobrenome é necessário!")]
        public string LastName { get; set; }

        [Display(Name = "Telefone (com DDD): *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Telefone é necessário!")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Display(Name = "Data de nascimento: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Data de nascimento é necessário!")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Birthday { get; set; }

        [Display(Name = "Senha: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Senha é necessária!")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Senha deve conter no minimo 6 digitos!")]
        [MaxLength(15, ErrorMessage = "Senha deve conter no máximo 15 digitos!")]
        public string Password { get; set; }

        [Display(Name = "Confirmar senha: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Confirmar senha é necessário!")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Senhas não são semelhantes!")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Foto de perfil: ")]
        public byte[] Image { get; set; }

        public bool TermsAndConditions { get; set; }
    }
}