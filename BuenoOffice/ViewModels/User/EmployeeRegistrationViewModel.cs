﻿using System.ComponentModel.DataAnnotations;

namespace BuenoOffice.ViewModels.User
{
    public class EmployeeRegistrationViewModel
    {
        [Display(Name = "Email do empregado: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email do empregado é necessário!")]
        public string EmployeeMail { get; set; }
    }
}