﻿using System.ComponentModel.DataAnnotations;

namespace BuenoOffice.ViewModels.User
{
    public class UserBaseEmail
    {
        [Display(Name = "Email: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email é necessário!")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}