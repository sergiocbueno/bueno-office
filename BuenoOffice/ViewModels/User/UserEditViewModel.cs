﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BuenoOffice.ViewModels.User
{
    public class UserEditViewModel : UserBaseEmail
    {
        [Display(Name = "Nome: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Nome é necessário!")]
        public string FirstName { get; set; }

        [Display(Name = "Sobrenome: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Sobrenome é necessário!")]
        public string LastName { get; set; }

        [Display(Name = "Telefone (com DDD): *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Telefone é necessário!")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Display(Name = "Data de nascimento: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Data de nascimento é necessário!")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime Birthday { get; set; }

        [Display(Name = "Alterar foto de perfil: ")]
        public byte[] Image { get; set; }
    }
}