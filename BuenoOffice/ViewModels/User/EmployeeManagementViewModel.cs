﻿using PagedList;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BuenoOffice.ViewModels.User
{
    public class EmployeeManagementViewModel : IPagedList
    {
        [Display(Name = "Nome do empregado: ")]
        public string EmployeeName { get; set; }

        public List<EmployeeDetailsViewModel> Data { get; set; }

        [Display(Name = "Paginação: ")]
        public int PageSize { get; set; }
        public int TotalItemCount { get; set; }
        public int PageNumber { get; set; }
        public int PageCount { get; set; }
        public bool HasPreviousPage { get; set; }
        public bool HasNextPage { get; set; }
        public bool IsFirstPage { get; set; }
        public bool IsLastPage { get; set; }
        public int FirstItemOnPage { get; set; }
        public int LastItemOnPage { get; set; }
    }
}