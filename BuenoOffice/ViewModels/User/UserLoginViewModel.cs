﻿using System.ComponentModel.DataAnnotations;

namespace BuenoOffice.ViewModels.User
{
    public class UserLoginViewModel : UserBaseEmail
    {
        [Display(Name = "Senha:")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Senha é necessária!")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Senha deve conter no minimo 6 digitos!")]
        [MaxLength(15, ErrorMessage = "Senha deve conter no máximo 15 digitos!")]
        public string Password { get; set; }

        [Display(Name = "Manter conectado ")]
        public bool RememberMe { get; set; }
    }
}