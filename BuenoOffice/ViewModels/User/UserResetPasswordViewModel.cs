﻿using System.ComponentModel.DataAnnotations;

namespace BuenoOffice.ViewModels.User
{
    public class UserResetPasswordViewModel : UserForgottenResetPasswordViewModel
    {
        [Display(Name = "Senha atual: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Senha atual é necessária!")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Senha deve conter no minimo 6 digitos!")]
        [MaxLength(15, ErrorMessage = "Senha deve conter no máximo 15 digitos!")]
        public string CurrentPassword { get; set; }

        public string Email { get; set; }
    }
}