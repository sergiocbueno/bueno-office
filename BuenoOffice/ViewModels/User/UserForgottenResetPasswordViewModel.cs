﻿using System.ComponentModel.DataAnnotations;

namespace BuenoOffice.ViewModels.User
{
    public class UserForgottenResetPasswordViewModel
    {
        [Display(Name = "Nova senha: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Nova senha é necessária!")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Senha deve conter no minimo 6 digitos!")]
        [MaxLength(15, ErrorMessage = "Senha deve conter no máximo 15 digitos!")]
        public string NewPassword { get; set; }

        [Display(Name = "Confirmar nova senha: *")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Confirmar nova senha é necessário!")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Senhas não são semelhantes!")]
        public string ConfirmNewPassword { get; set; }

        public string Guid { get; set; }
    }
}