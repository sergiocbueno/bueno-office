﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BuenoOffice.ViewModels.User
{
    public class EmployeeDetailsViewModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Phone { get; set; }

        [Required]
        public DateTime Birthday { get; set; }
    }
}