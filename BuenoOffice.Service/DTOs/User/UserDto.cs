﻿using System;
using BuenoOffice.Infrastructure.Utilities.Enums.User;

namespace BuenoOffice.Service.DTOs.User
{
    public class UserDto
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public DateTime Birthday { get; set; }
        public string Password { get; set; }
        public bool IsEmailVerified { get; set; }
        public Guid ActivationCode { get; set; }
        public bool HasForgotten { get; set; }
        public UserType Type { get; set; }
        public byte[] Image { get; set; }
    }
}
