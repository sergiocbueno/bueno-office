﻿namespace BuenoOffice.Service.DTOs.User
{
    public class UserResetPasswordDto : UserForgottenResetPasswordDto
    {
        public string CurrentPassword { get; set; }
        public string Email { get; set; }
    }
}
