﻿namespace BuenoOffice.Service.DTOs.User
{
    public class UserForgottenResetPasswordDto
    {
        public string NewPassword { get; set; }
        public string ConfirmNewPassword { get; set; }
        public string Guid { get; set; }
    }
}
