﻿namespace BuenoOffice.Service.DTOs.Contact
{
    public class ContactMessageDto
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Car { get; set; }
        public int? Phone { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }
}
