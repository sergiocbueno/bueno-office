﻿namespace BuenoOffice.Service.DTOs.Conversion
{
    public class ConversionDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
