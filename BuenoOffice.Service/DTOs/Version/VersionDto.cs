﻿using BuenoOffice.Service.DTOs.Model;

namespace BuenoOffice.Service.DTOs.Version
{
    public class VersionDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ModelDto Model { get; set; }
    }
}
