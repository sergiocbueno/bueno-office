﻿using System;
using BuenoOffice.Infrastructure.Utilities.Enums.Faq;

namespace BuenoOffice.Service.DTOs.Faq
{
    public class FaqDto
    {
        public int Id { get; set; }
        public FaqStatus Status { get; set; }
        public string Question { get; set; }
        public DateTime? QuestionDate { get; set; }
        public string Answer { get; set; }
        public DateTime? AnswerDate { get; set; }
    }
}
