﻿namespace BuenoOffice.Service.DTOs.Color
{
    public class ColorDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
