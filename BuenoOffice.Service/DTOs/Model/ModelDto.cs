﻿using BuenoOffice.Service.DTOs.Brand;

namespace BuenoOffice.Service.DTOs.Model
{
    public class ModelDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual BrandDto Brand { get; set; }
    }
}
