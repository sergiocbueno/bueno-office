﻿using System;
using BuenoOffice.Service.DTOs.Color;
using BuenoOffice.Service.DTOs.Conversion;
using BuenoOffice.Service.DTOs.User;
using BuenoOffice.Service.DTOs.Version;

namespace BuenoOffice.Service.DTOs.Car
{
    public class CarDto
    {
        public int Id { get; set; }
        public virtual VersionDto Version { get; set; }
        public virtual ConversionDto Conversion { get; set; }
        public virtual ColorDto Color { get; set; }
        public int ModelYear { get; set; }
        public int ManufactureYear { get; set; }
        public int? Mileage { get; set; }
        public virtual UserDto CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
