﻿namespace BuenoOffice.Service.DTOs.Car
{
    public class NewCarDto
    {
        public int ModelYear { get; set; }
        public int ManufactureYear { get; set; }
        public int? Mileage { get; set; }
        public int SelectColorId { get; set; }
        public int SelectConversionId { get; set; }
        public int SelectVersionId { get; set; }
    }
}
