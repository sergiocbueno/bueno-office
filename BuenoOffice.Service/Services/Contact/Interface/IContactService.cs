﻿using BuenoOffice.Service.DTOs.Contact;

namespace BuenoOffice.Service.Services.Contact.Interface
{
    public interface IContactService
    {
        void SendMessage(ContactMessageDto contactMessage);
        bool IsValidEmail(string email);
        bool IsValidPhone(string phone);
    }
}
