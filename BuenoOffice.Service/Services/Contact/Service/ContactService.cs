﻿using System;
using System.Text.RegularExpressions;
using BuenoOffice.Infrastructure.Log;
using BuenoOffice.Infrastructure.Utilities.Mail.Interface;
using BuenoOffice.Infrastructure.Utilities.Mail.Model;
using BuenoOffice.Service.DTOs.Contact;
using BuenoOffice.Service.Services.Contact.Interface;
using Microsoft.Practices.Unity;

namespace BuenoOffice.Service.Services.Contact.Service
{
    public class ContactService : IContactService
    {
        private readonly IUnityContainer container;

        public ContactService(IUnityContainer container)
        {
            this.container = container;
        }

        public void SendMessage(ContactMessageDto contactMessage)
        {
            var smtpMail = container.Resolve<ISmtpMail>();

            var body = "Nome: " + contactMessage.Name + "<br/>" +
                       "Carro: " + (contactMessage.Car == null || contactMessage.Car.Equals(string.Empty) ? "* Carro não informado!" : contactMessage.Car) + "<br/>" +
                       "Telefone: " + (contactMessage.Phone == null ? "* Telefone não informado!" : contactMessage.Phone.ToString()) + "<br/>" +
                       "Pergunta: " + contactMessage.Message;

            var mail = new Mail
            {
                ToMail = null,
                Subject = contactMessage.Subject,
                Body = body,
                CcMail = contactMessage.Email
            };

            Logger.Logging.Info("Send mail from contact view!");

            smtpMail.SendMailBySmtpProtocol(mail);
        }

        public bool IsValidEmail(string email)
        {
            var regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            return regex.Match(email).Success;
        }

        public bool IsValidPhone(string phone)
        {
            var regex = new Regex(@"^\([1-9]{2}\) [2-9][0-9]{3,4}\-[0-9]{4}$");
            return regex.Match(phone).Success;
        }
    }
}
