﻿using System.Collections.Generic;
using System.Web.Mvc;
using BuenoOffice.Infrastructure.Utilities.Enums.Faq;
using BuenoOffice.Service.DTOs.Faq;
using BuenoOffice.Service.Helpers.Paged;

namespace BuenoOffice.Service.Services.Faq.Interface
{
    public interface IFaqService
    {
        FaqDto CreateOrUpdate(FaqDto faqDto);
        FaqDto DisableFaq(int faqId);
        List<FaqDto> SearchQuestions(string partOfQuestions);
        List<FaqDto> LastQuestions();
        PagedList<FaqDto> GetPagedFaqByQuestionAndStatus(string search, FaqStatus status, int page, int pageSize);
        IEnumerable<SelectListItem> GetAllFaqStatus();
        FaqDto GetFaqById(int id);
    }
}
