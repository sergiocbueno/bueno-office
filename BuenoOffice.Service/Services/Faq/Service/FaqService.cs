﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BuenoOffice.Data.Entities.Faq.Interface;
using BuenoOffice.Data.Parametrization.Context;
using BuenoOffice.Infrastructure.Log;
using BuenoOffice.Infrastructure.Utilities.Enums.Faq;
using BuenoOffice.Service.DTOs.Faq;
using BuenoOffice.Service.Helpers.Enum;
using BuenoOffice.Service.Helpers.Paged;
using BuenoOffice.Service.Mapping;
using BuenoOffice.Service.Services.Faq.Interface;
using Microsoft.Practices.Unity;

namespace BuenoOffice.Service.Services.Faq.Service
{
    public class FaqService : IFaqService
    {
        private readonly IUnityContainer container;

        public FaqService(IUnityContainer container)
        {
            this.container = container;
        }

        
        public virtual FaqDto CreateOrUpdate (FaqDto faqDto)
        {
            var context = container.Resolve<IParametrizationContext>();
            var faqRepository = container.Resolve<IFaqRepository>();
            var mapper = BusinessMapper.ConfigureMapper();

            Logger.Logging.Info($"Create or update faq. New : [{faqDto.Id.Equals(0)}]");

            var faq = faqRepository.Get(x => x.Id.Equals(faqDto.Id)).FirstOrDefault() ?? new Data.Entities.Faq.Model.Faq();

            faq.Status = faq.Id.Equals(0) ? FaqStatus.Created : FaqStatus.Answered;
            faq.Question = faq.Question ?? faqDto.Question;
            faq.QuestionDate = faq.QuestionDate == null ? DateTime.Now : faqDto.QuestionDate;
            faq.Answer = faq.Answer ?? faqDto.Answer;
            faq.AnswerDate = faq.AnswerDate ?? faqDto.AnswerDate;

            faqRepository.SaveOrUpdate(faq);
            context.SaveChanges();

            Logger.Logging.Info($"Faq Id : [{faq.Id}] saved successfully");

            faqDto = mapper.Map<FaqDto>(faq);
			return faqDto;
		}

        public FaqDto DisableFaq(int faqId)
        {
            var context = container.Resolve<IParametrizationContext>();
            var faqRepository = container.Resolve<IFaqRepository>();
            var mapper = BusinessMapper.ConfigureMapper();

            var faq = faqRepository.Get(faqId);
            faq.Status = FaqStatus.Disabled;

            faqRepository.SaveOrUpdate(faq);
            context.SaveChanges();

            Logger.Logging.Info($"Faq Id : [{faq.Id}] disabled successfully");

            var faqDto = mapper.Map<FaqDto>(faq);
            return faqDto;
        }

        public List<FaqDto> SearchQuestions(string partOfQuestions)
        {
            var mapper = BusinessMapper.ConfigureMapper();
            var faqRepository = container.Resolve<IFaqRepository>();
            var faqs = faqRepository.Get(x => x.Question.Contains(partOfQuestions) && x.Status == FaqStatus.Answered).Take(6).ToList();
            var faqDtos = mapper.Map<List<FaqDto>>(faqs);

            Logger.Logging.Info($"Service return a list with [{faqDtos.Count}] question(s) for faq page");
            return faqDtos;
        }

        public List<FaqDto> LastQuestions()
        {
            var mapper = BusinessMapper.ConfigureMapper();
            var faqRepository = container.Resolve<IFaqRepository>();
            var faqs = faqRepository.Get(x => x.Status == FaqStatus.Answered).OrderByDescending(x => x.Id).Take(6).ToList();
            var faqDtos = mapper.Map<List<FaqDto>>(faqs);

            Logger.Logging.Info($"Service return a list with [{faqDtos.Count}] question(s) for faq page");
            return faqDtos;
        }

        public PagedList<FaqDto> GetPagedFaqByQuestionAndStatus(string search, FaqStatus status, int page, int pageSize)
        {
            var mapper = BusinessMapper.ConfigureMapper();
            var faqRepository = container.Resolve<IFaqRepository>();

            search = search ?? string.Empty;
            var skip = page * pageSize - pageSize;
            var allFaqs = faqRepository.Get(x => x.Question.Contains(search) && x.Status == status).OrderBy(x => x.Id);
            var totalCount = allFaqs.Count();

            var faqs = allFaqs
                .Skip(skip)
                .Take(pageSize)
                .ToList();
            var faqDtos = mapper.Map<List<FaqDto>>(faqs);
            var pagedList = new PagedList<FaqDto>(faqDtos, page, pageSize, totalCount);

            Logger.Logging.Info($"Service return a list with [{faqDtos.Count}] question(s) for manager faq page. Page [{page}]");
            return pagedList;
        }

        public IEnumerable<SelectListItem> GetAllFaqStatus()
        {
            var selectListItems = new List<SelectListItem>();

            for (int index = 0; index <= 2; index++)
            {
                var item = new SelectListItem { Text = EnumHelper.GetEnumDescription((FaqStatus)index), Value = index.ToString() };
                selectListItems.Add(item);
            }
            
            return selectListItems;
        }

        public FaqDto GetFaqById(int id)
        {
            var faqRepository = container.Resolve<IFaqRepository>();
            var mapper = BusinessMapper.ConfigureMapper();

            var faq = faqRepository.Get(id);
            var faqDto = mapper.Map<FaqDto>(faq);

            return faqDto;
        }
    }
}
