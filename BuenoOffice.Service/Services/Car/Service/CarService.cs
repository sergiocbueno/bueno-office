﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BuenoOffice.Data.Entities.Brand.Interface;
using BuenoOffice.Data.Entities.Car.Interface;
using BuenoOffice.Data.Entities.CarModel.Interface;
using BuenoOffice.Data.Entities.Color.Interface;
using BuenoOffice.Data.Entities.Conversion.Interface;
using BuenoOffice.Data.Entities.User.Interface;
using BuenoOffice.Data.Entities.Version.Interface;
using BuenoOffice.Data.Parametrization.Context;
using BuenoOffice.Infrastructure.Log;
using BuenoOffice.Service.DTOs.Car;
using BuenoOffice.Service.Mapping;
using BuenoOffice.Service.Services.Car.Interface;
using Microsoft.Practices.Unity;

namespace BuenoOffice.Service.Services.Car.Service
{
    public class CarService : ICarService
    {
        private readonly IUnityContainer container;

        public CarService(IUnityContainer container)
        {
            this.container = container;
        }

        public List<CarDto> GetUserCarsByUserMail(string userMail)
        {
            var userRepository = container.Resolve<IUserRepository>();
            var carRepository = container.Resolve<ICarRepository>();
            var mapper = BusinessMapper.ConfigureMapper();

            var user = userRepository.Get(x => x.Email.Equals(userMail)).SingleOrDefault();

            if (user != null)
            {
                var cars = carRepository.Get(x => x.CreatedBy.Id.Equals(user.Id)).ToList();
                var carsDto = mapper.Map<List<CarDto>>(cars);
                return carsDto;
            }

            return null;
        }

        public void CreateCar(NewCarDto newCar, string userMail)
        {
            var userRepository = container.Resolve<IUserRepository>();
            var user = userRepository.Get(x => x.Email.Equals(userMail)).SingleOrDefault();

            if (user != null)
            {
                var carRepository = container.Resolve<ICarRepository>();
                var versionRepository = container.Resolve<IVersionRepository>();
                var conversionRepository = container.Resolve<IConversionRepository>();
                var colorRepository = container.Resolve<IColorRepository>();
                var context = container.Resolve<IParametrizationContext>();

                var car = new Data.Entities.Car.Model.Car
                {
                    Version = versionRepository.Get(newCar.SelectVersionId),
                    Conversion = conversionRepository.Get(newCar.SelectConversionId),
                    Color = colorRepository.Get(newCar.SelectColorId),
                    ModelYear = newCar.ModelYear,
                    ManufactureYear = newCar.ManufactureYear,
                    Mileage = newCar.Mileage,
                    CreatedBy = user,
                    CreatedOn = DateTime.Now,
                    UpdatedOn = null
                };

                carRepository.SaveOrUpdate(car);
                context.SaveChanges();
                Logger.Logging.Info($"Create new car. Id : [{car.Id}]");
            }
            else
            {
                Logger.Logging.Info($"Not allowed to create new car. Invalid user mail: [{userMail}]");
            }
            
        }

        public IEnumerable<SelectListItem> GetAllColors()
        {
            var colorRepository = container.Resolve<IColorRepository>();

            var selectListItems = new List<SelectListItem>();
            var colors = colorRepository.GetAll().ToList();

            foreach (var color in colors)
            {
                var item = new SelectListItem { Text = color.Name, Value = color.Id.ToString() };
                selectListItems.Add(item);
            }

            return selectListItems;
        }

        public IEnumerable<SelectListItem> GetAllConversions()
        {
            var conversionRepository = container.Resolve<IConversionRepository>();

            var selectListItems = new List<SelectListItem>();
            var conversions = conversionRepository.GetAll().ToList();

            foreach (var conversion in conversions)
            {
                var item = new SelectListItem { Text = conversion.Name, Value = conversion.Id.ToString() };
                selectListItems.Add(item);
            }

            return selectListItems;
        }

        public IEnumerable<SelectListItem> GetAllBrands()
        {
            var brandRepository = container.Resolve<IBrandRepository>();

            var selectListItems = new List<SelectListItem>();
            var brands = brandRepository.GetAll().ToList();

            foreach (var brand in brands)
            {
                var item = new SelectListItem {Text = brand.Name, Value = brand.Id.ToString()};
                selectListItems.Add(item);
            }

            return selectListItems;
        }

        public IEnumerable<SelectListItem> GetAllModelsByBrand (int brandId)
        {
            var modelRepository = container.Resolve<IModelRepository>();

            var selectListItems = new List<SelectListItem>();
            var models = modelRepository.Get(x => x.Brand.Id.Equals(brandId)).ToList();

            foreach (var model in models)
            {
                var item = new SelectListItem { Text = model.Name, Value = model.Id.ToString() };
                selectListItems.Add(item);
            }

            return selectListItems;
        }

        public IEnumerable<SelectListItem> GetAllVersionsByModel(int modelId)
        {
            var versionRepository = container.Resolve<IVersionRepository>();

            var selectListItems = new List<SelectListItem>();
            var versions = versionRepository.Get(x => x.Model.Id.Equals(modelId)).ToList();

            foreach (var version in versions)
            {
                var item = new SelectListItem { Text = version.Name, Value = version.Id.ToString() };
                selectListItems.Add(item);
            }

            return selectListItems;
        }
    }
}
