﻿using System.Collections.Generic;
using System.Web.Mvc;
using BuenoOffice.Service.DTOs.Car;

namespace BuenoOffice.Service.Services.Car.Interface
{
    public interface ICarService
    {
        List<CarDto> GetUserCarsByUserMail(string userMail);
        void CreateCar(NewCarDto newCar, string userMail);
        IEnumerable<SelectListItem> GetAllColors();
        IEnumerable<SelectListItem> GetAllConversions();
        IEnumerable<SelectListItem> GetAllBrands();
        IEnumerable<SelectListItem> GetAllModelsByBrand(int brandId);
        IEnumerable<SelectListItem> GetAllVersionsByModel(int modelId);
    }
}
