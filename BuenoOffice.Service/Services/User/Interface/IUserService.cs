﻿using BuenoOffice.Service.DTOs.User;
using BuenoOffice.Service.Helpers.Paged;
using BuenoOffice.Service.Helpers.User;

namespace BuenoOffice.Service.Services.User.Interface
{
    public interface IUserService
    {
        void CreateUser(UserDto userDto);
        void UpdateUser(UserDto userDto);
        void CreateEmployee(string name);
        UserContext LoginActivedUser(string email, string password);
        bool VerifyAccount(string id);
        bool IsValidMail(string email);
        bool RequestNewPassword(string email);
        bool VerifyNewPassword(string id);
        bool ForgottenResetPassword(UserForgottenResetPasswordDto setting);
        bool ResetPassword(UserResetPasswordDto setting);
        UserDto GetUserByEmail(string email);
        PagedList<UserDto> GetPagedEmployeeUserByName(string employeeName, int page, int pageSize);
        void RemoveEmployeeById(int employeeId);
    }
}
