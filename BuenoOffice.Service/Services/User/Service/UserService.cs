﻿using System;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using BuenoOffice.Data.Entities.User.Interface;
using BuenoOffice.Data.Parametrization.Context;
using BuenoOffice.Infrastructure.Log;
using BuenoOffice.Infrastructure.Utilities.Cryptography;
using BuenoOffice.Infrastructure.Utilities.Enums.User;
using BuenoOffice.Infrastructure.Utilities.Mail.Interface;
using BuenoOffice.Infrastructure.Utilities.Mail.Model;
using BuenoOffice.Service.DTOs.User;
using BuenoOffice.Service.Helpers.User;
using BuenoOffice.Service.Mapping;
using BuenoOffice.Service.Services.User.Interface;
using Microsoft.Practices.Unity;
using BuenoOffice.Service.Helpers.Paged;
using System.Collections.Generic;

namespace BuenoOffice.Service.Services.User.Service
{
    public class UserService : IUserService
    {
        private IUnityContainer container;

        public UserService(IUnityContainer container)
        {
            this.container = container;
        }

        public void CreateUser(UserDto userDto)
        {
            var context = container.Resolve<IParametrizationContext>();
            var userRepository = container.Resolve<IUserRepository>();

            var user = userRepository.Get(x => x.Email.Equals(userDto.Email) && x.Type == UserType.Client).SingleOrDefault();

            if (user == null)
            {
                user = new Data.Entities.User.Model.User();
                CreateUser(user, userDto);
                SendActivateLink(user);

                userRepository.SaveOrUpdate(user);
                context.SaveChanges();
                Logger.Logging.Info($"Create new user successfully. Email: [{userDto.Email}]");
            }
            else
            {
                Logger.Logging.Error($"Invalid email: [{userDto.Email}]. It's already been used on system.");
            }
        }

        public void UpdateUser(UserDto userDto)
        {
            var context = container.Resolve<IParametrizationContext>();
            var userRepository = container.Resolve<IUserRepository>();

            var user = userRepository.Get(x => x.Email.Equals(userDto.Email)).SingleOrDefault();

            if (user != null)
            {
                UpdateUser(user, userDto);

                userRepository.SaveOrUpdate(user);
                context.SaveChanges();
                Logger.Logging.Info($"Update user successfully. Email : [{userDto.Email}]");
            }
            else
            {
                Logger.Logging.Error($"Invalid user email: [{userDto.Email}]. It's not exist on system to be updated.");
            }
            
        }

        public void CreateEmployee(string employeeMail)
        {
            if (!string.IsNullOrEmpty(employeeMail))
            {
                var context = container.Resolve<IParametrizationContext>();
                var userRepository = container.Resolve<IUserRepository>();

                var user = new Data.Entities.User.Model.User
                {
                    ActivationCode = new Guid(),
                    Email = employeeMail,
                    FirstName = "Alterar nome",
                    LastName = "Alterar sobrenome",
                    Birthday = DateTime.Now,
                    IsEmailVerified = true,
                    Type = UserType.Employee,
                    Password = Cryptography.Hash("mecanicabueno"),
                    Phone = "11999999999"
                };

                userRepository.SaveOrUpdate(user);
                context.SaveChanges();
                Logger.Logging.Info($"Create new employee. Email: [{user.Email}]");
            }
            else
            {
                Logger.Logging.Error("Invalid employee mail during create employee.");
            }
        }

        public UserContext LoginActivedUser(string email, string password)
        {
            var userRepository = container.Resolve<IUserRepository>();
            var passwordWithHash = Cryptography.Hash(password);
            var result = userRepository.Get(user => user.Email.Equals(email) &&
                                                  user.Password.Equals(passwordWithHash) && 
                                                  user.IsEmailVerified).SingleOrDefault();

            return result == null
                ? null
                : new UserContext(result.Email, result.Type);
        }

        public bool VerifyAccount(string id)
        {
            var context = container.Resolve<IParametrizationContext>();
            var userRepository = container.Resolve<IUserRepository>();

            var user = userRepository.Get(usr => usr.ActivationCode == new Guid(id) && usr.IsEmailVerified == false).SingleOrDefault();

            if (user != null)
            {
                user.IsEmailVerified = true;
                userRepository.SaveOrUpdate(user);
                context.SaveChanges();
                return true;
            }

            return false;
        }

        public bool IsValidMail(string email)
        {
            var userRepository = container.Resolve<IUserRepository>();

            if (IsValidEmailStructure(email).Success)
            {
                var user = userRepository.Get(x => x.Email.Equals(email)).SingleOrDefault();
                return user == null;
            }
            
            return false;
        }

        public bool RequestNewPassword(string email)
        {
            var userRepository = container.Resolve<IUserRepository>();
            var context = container.Resolve<IParametrizationContext>();

            if (IsValidEmailStructure(email).Success)
            {
                var user = userRepository.Get(x => x.Email.Equals(email)).SingleOrDefault();

                if (user != null)
                {
                    user.HasForgotten = true;
                    user.ActivationCode = Guid.NewGuid();
                    userRepository.SaveOrUpdate(user);
                    context.SaveChanges();

                    SendResetPasswordLink(user);
                }

                return true;
            }

            return false;
        }

        public bool VerifyNewPassword (string id)
        {
            var userRepository = container.Resolve<IUserRepository>();
            var user = userRepository.Get(usr => usr.ActivationCode == new Guid(id) && usr.HasForgotten).SingleOrDefault();
            return user != null;
        }

        public bool ForgottenResetPassword (UserForgottenResetPasswordDto setting)
        {
            var userRepository = container.Resolve<IUserRepository>();
            var user = userRepository.Get(usr => usr.ActivationCode == new Guid(setting.Guid) && usr.HasForgotten).SingleOrDefault();

            if (user != null)
            {
                var context = container.Resolve<IParametrizationContext>();

                user.Password = Cryptography.Hash(setting.NewPassword);
                user.HasForgotten = false;
                userRepository.SaveOrUpdate(user);
                context.SaveChanges();

                return true;
            }

            return false;
        }

        public bool ResetPassword (UserResetPasswordDto setting)
        {
            var userRepository = container.Resolve<IUserRepository>();

            var currentPassword = Cryptography.Hash(setting.CurrentPassword);
            var user = userRepository.Get(usr => usr.Email == setting.Email && usr.Password == currentPassword).SingleOrDefault();

            if (user != null)
            {
                var context = container.Resolve<IParametrizationContext>();

                user.Password = Cryptography.Hash(setting.NewPassword);
                userRepository.SaveOrUpdate(user);
                context.SaveChanges();

                return true;
            }

            return false;
        }

        public UserDto GetUserByEmail(string email)
        {
            var userRepository = container.Resolve<IUserRepository>();
            var mapper = BusinessMapper.ConfigureMapper();

            var user = userRepository.Get(usr => usr.Email.Equals(email) && usr.IsEmailVerified).SingleOrDefault();

            return user != null ? mapper.Map<UserDto>(user) : null;
        }

        public PagedList<UserDto> GetPagedEmployeeUserByName(string employeeName, int page, int pageSize)
        {
            var mapper = BusinessMapper.ConfigureMapper();
            var userRepository = container.Resolve<IUserRepository>();

            employeeName = employeeName ?? string.Empty;
            var skip = page * pageSize - pageSize;
            var allEmployeeUsers = userRepository.Get(x => (x.FirstName.Contains(employeeName) || x.LastName.Contains(employeeName)) && x.Type == UserType.Employee).OrderBy(x => x.Id);
            var totalCount = allEmployeeUsers.Count();

            var employeeUsers = allEmployeeUsers
                .Skip(skip)
                .Take(pageSize)
                .ToList();
            var employeeUsersDtos = mapper.Map<List<UserDto>>(employeeUsers);
            var pagedList = new PagedList<UserDto>(employeeUsersDtos, page, pageSize, totalCount);

            Logger.Logging.Info($"Service return a list with [{employeeUsersDtos.Count}] employee(s) for manager employee page. Page [{page}]");
            return pagedList;
        }

        public void RemoveEmployeeById (int employeeId)
        {
            var userRepository = container.Resolve<IUserRepository>();
            var context = container.Resolve<IParametrizationContext>();

            userRepository.Delete(employeeId);
            context.SaveChanges();
            Logger.Logging.Info($"Employee Id - [{employeeId}] removed successfully!");
        }

        #region Private Methods

        private Match IsValidEmailStructure(string email)
        {
            var regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            return regex.Match(email);
        }

        private void CreateUser(Data.Entities.User.Model.User user, UserDto userDto)
        {
            user.Email = userDto.Email;
            user.FirstName = userDto.FirstName;
            user.LastName = userDto.LastName;
            user.Phone = userDto.Phone;
            user.Birthday = userDto.Birthday;
            user.Password = Cryptography.Hash(userDto.Password);
            user.IsEmailVerified = false;
            user.ActivationCode = Guid.NewGuid();
            user.HasForgotten = false;
            user.Type = UserType.Client;
            user.Image = userDto.Image;
        }

        private void UpdateUser(Data.Entities.User.Model.User user, UserDto userDto)
        {
            user.FirstName = userDto.FirstName ?? user.FirstName;
            user.LastName = userDto.LastName ?? user.LastName;
            user.Phone = userDto.Phone ?? user.Phone;
            user.Birthday = userDto.Birthday;
            user.Image = userDto.Image ?? user.Image;
        }

        private void SendActivateLink(Data.Entities.User.Model.User user)
        {
            var baseUrl = ConfigurationManager.AppSettings["BaseUrl"];
            var smtpMail = container.Resolve<ISmtpMail>();

            var link = baseUrl + "/User/VerifyAccount/" + user.ActivationCode;
            var body =
                "Falta pouco .. Para ativar sua conta e poder usufruir de todos os beneficios do sistema da mecanica bueno online"
                + "<br/><br/>Acesse o link: <a href='" + link + "'>" + link + "</a>";

            var mail = new Mail
            {
                ToMail = user.Email,
                Subject = "[Mecânica Bueno] Link de ativação de conta",
                Body = body
            };

            Logger.Logging.Info($"Send mail to [{user.Email}] with activate link");

            smtpMail.SendMailBySmtpProtocol(mail);
        }

        private void SendResetPasswordLink(Data.Entities.User.Model.User user)
        {
            var baseUrl = ConfigurationManager.AppSettings["BaseUrl"];
            var smtpMail = container.Resolve<ISmtpMail>();

            var link = baseUrl + "/User/VerifyNewPassword/" + user.ActivationCode;
            var body =
                "Falta pouco .. Para resetar a sua senha e poder usufruir novamente de todos os beneficios do sistema da mecanica bueno online"
                + "<br/><br/>Acesse o link: <a href='" + link + "'>" + link + "</a>";

            var mail = new Mail
            {
                ToMail = user.Email,
                Subject = "[Mecânica Bueno] Link para resetar senha",
                Body = body
            };

            Logger.Logging.Info($"Send mail to [{user.Email}] with reset password link");

            smtpMail.SendMailBySmtpProtocol(mail);
        }

        #endregion

    }
}