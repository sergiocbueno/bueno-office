﻿using AutoMapper;
using BuenoOffice.Service.DTOs.Car;

namespace BuenoOffice.Service.Mapping.Profiles.Car
{
    public class CarServiceMappingProfile : Profile
    {
        public CarServiceMappingProfile()
        {
            CreateMap<Data.Entities.Car.Model.Car, CarDto>();
        }
    }
}
