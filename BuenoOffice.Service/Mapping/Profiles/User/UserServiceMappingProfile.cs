﻿using AutoMapper;
using BuenoOffice.Service.DTOs.User;

namespace BuenoOffice.Service.Mapping.Profiles.User
{
    public class UserServiceMappingProfile : Profile
    {
        public UserServiceMappingProfile()
        {
            CreateMap<Data.Entities.User.Model.User, UserDto>();
        }
    }
}
