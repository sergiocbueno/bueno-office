﻿using AutoMapper;
using BuenoOffice.Service.DTOs.Version;

namespace BuenoOffice.Service.Mapping.Profiles.Version
{
    public class VersionServiceMappingProfile : Profile
    {
        public VersionServiceMappingProfile()
        {
            CreateMap<Data.Entities.Version.Model.Version, VersionDto>();
        }
    }
}
