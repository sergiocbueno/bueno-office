﻿using AutoMapper;
using BuenoOffice.Service.DTOs.Color;

namespace BuenoOffice.Service.Mapping.Profiles.Color
{
    public class ColorServiceMappingProfile : Profile
    {
        public ColorServiceMappingProfile()
        {
            CreateMap<Data.Entities.Color.Model.Color, ColorDto>();
        }
    }
}
