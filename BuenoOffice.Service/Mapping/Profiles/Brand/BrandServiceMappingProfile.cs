﻿using AutoMapper;
using BuenoOffice.Service.DTOs.Brand;

namespace BuenoOffice.Service.Mapping.Profiles.Brand
{
    public class BrandServiceMappingProfile : Profile
    {
        public BrandServiceMappingProfile()
        {
            CreateMap<Data.Entities.Brand.Model.Brand, BrandDto>();
        }
    }
}
