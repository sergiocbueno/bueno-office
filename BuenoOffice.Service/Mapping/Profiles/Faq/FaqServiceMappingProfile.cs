﻿using AutoMapper;
using BuenoOffice.Service.DTOs.Faq;

namespace BuenoOffice.Service.Mapping.Profiles.Faq
{
    public class FaqServiceMappingProfile : Profile
    {
        public FaqServiceMappingProfile()
        {
            CreateMap<Data.Entities.Faq.Model.Faq, FaqDto>();
        }
    }
}
