﻿using AutoMapper;
using BuenoOffice.Service.DTOs.Model;

namespace BuenoOffice.Service.Mapping.Profiles.Model
{
    public class ModelServiceMappingProfile : Profile
    {
        public ModelServiceMappingProfile()
        {
            CreateMap<Data.Entities.CarModel.Model.Model, ModelDto>();
        }
    }
}
