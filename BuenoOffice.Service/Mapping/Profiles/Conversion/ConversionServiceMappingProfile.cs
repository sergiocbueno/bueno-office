﻿using AutoMapper;
using BuenoOffice.Service.DTOs.Conversion;

namespace BuenoOffice.Service.Mapping.Profiles.Conversion
{
    public class ConversionServiceMappingProfile : Profile
    {
        public ConversionServiceMappingProfile()
        {
            CreateMap<Data.Entities.Conversion.Model.Conversion, ConversionDto>();
        }
    }
}
