﻿using AutoMapper;
using BuenoOffice.Service.Mapping.Profiles.Brand;
using BuenoOffice.Service.Mapping.Profiles.Car;
using BuenoOffice.Service.Mapping.Profiles.Color;
using BuenoOffice.Service.Mapping.Profiles.Conversion;
using BuenoOffice.Service.Mapping.Profiles.Faq;
using BuenoOffice.Service.Mapping.Profiles.Model;
using BuenoOffice.Service.Mapping.Profiles.User;
using BuenoOffice.Service.Mapping.Profiles.Version;

namespace BuenoOffice.Service.Mapping
{
    public static class BusinessMapper
    {
        private static bool _isMapped;
        private static IMapper _mapper;

        public static IMapper ConfigureMapper()
        {
            if (!_isMapped)
            {
                _mapper = new MapperConfiguration(map =>
                {
                    map.AddProfile<UserServiceMappingProfile>();
                    map.AddProfile<FaqServiceMappingProfile>();
                    map.AddProfile<BrandServiceMappingProfile>();
                    map.AddProfile<ColorServiceMappingProfile>();
                    map.AddProfile<ConversionServiceMappingProfile>();
                    map.AddProfile<ModelServiceMappingProfile>();
                    map.AddProfile<VersionServiceMappingProfile>();
                    map.AddProfile<CarServiceMappingProfile>();
                }).CreateMapper();


                _isMapped = true;
            }

            return _mapper;
        }
    }
}
