﻿using System.Security.Principal;
using BuenoOffice.Infrastructure.Utilities.Enums.User;
using BuenoOffice.Service.Helpers.Enum;

namespace BuenoOffice.Service.Helpers.User
{
    public class UserContext : IPrincipal
    {
        public IIdentity Identity { get; }
        public string Email { get; set; }
        public string Role { get; set; }

        public UserContext(string email, UserType role)
        {
            Email = email;
            Role = EnumHelper.GetEnumDescription(role);
        }

        public bool IsInRole(string role)
        {
            return Role.Equals(role);
        }
    }
}
