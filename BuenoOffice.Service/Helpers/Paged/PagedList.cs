﻿using System;
using System.Collections.Generic;
using PagedList;

namespace BuenoOffice.Service.Helpers.Paged
{
    public class PagedList<T> : IPagedList
    {
        public List<T> Data { get; }
        public int TotalItemCount { get; }
        public int PageNumber { get; }
        public int PageSize { get; }
        public int PageCount => (int)Math.Ceiling((double)TotalItemCount / PageSize);
        public bool HasPreviousPage => PageNumber > 1;
        public bool HasNextPage => PageNumber + 1 <= PageCount;
        public bool IsFirstPage => PageNumber == 1;
        public bool IsLastPage => PageNumber == PageCount;
        public int FirstItemOnPage { get; }
        public int LastItemOnPage { get; }

        public PagedList(List<T> source, int page, int pageSize, int totalCount)
        {
            Data = source;
            PageNumber = page;
            PageSize = pageSize;
            TotalItemCount = totalCount;
        }
    }
}
