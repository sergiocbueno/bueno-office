﻿using System.ComponentModel;

namespace BuenoOffice.Service.Helpers.Enum
{
    public static class EnumHelper
    {
        public static string GetEnumDescription(System.Enum option)
        {
            var type = option.GetType();
            var memberInfos = type.GetMember(option.ToString());

            if (memberInfos.Length > 0)
            {
                var attributes = memberInfos[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes.Length > 0)
                {
                    return ((DescriptionAttribute)attributes[0]).Description;
                }
            }

            return "";
        }

    }
}
