﻿using System.Reflection;

namespace BuenoOffice.Infrastructure.Log
{
    public static class Logger
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static log4net.ILog Logging => Log;
    }
}