﻿using System.ComponentModel;

namespace BuenoOffice.Infrastructure.Utilities.Enums.User
{
    public enum UserType
    {
        [Description("Administrador")]
        Manager,
        [Description("Empregado")]
        Employee,
        [Description("Cliente")]
        Client
    }
}
