﻿using System.ComponentModel;

namespace BuenoOffice.Infrastructure.Utilities.Enums.Faq
{
    public enum FaqStatus
    {
        [Description("Não Respondido")]
        Created,
        [Description("Respondido")]
        Answered,
        [Description("Desativado")]
        Disabled
    }
}
