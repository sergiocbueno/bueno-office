﻿using System;
using System.Text;

namespace BuenoOffice.Infrastructure.Utilities.Cryptography
{
    public static class Cryptography
    {
        public static string Hash(string password)
        {
            var passwordWithHash = Convert.ToBase64String(System.Security.Cryptography.SHA256.Create()
                .ComputeHash(Encoding.UTF8.GetBytes(password)));
            return passwordWithHash;
        }
    }
}
