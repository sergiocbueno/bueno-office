﻿using System;
using System.Net;
using System.Net.Mail;
using BuenoOffice.Infrastructure.Utilities.Mail.Interface;
using System.Configuration;
using BuenoOffice.Infrastructure.Log;

namespace BuenoOffice.Infrastructure.Utilities.Mail.Service
{
    public class SmtpMail : ISmtpMail
    {
        public void SendMailBySmtpProtocol(Model.Mail mail)
        {
            var fromMail = ConfigurationManager.AppSettings["SmtpUserName"];
            var password = ConfigurationManager.AppSettings["SmtpUserPassword"];
            var host = ConfigurationManager.AppSettings["SmtpHost"];
            var port = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]);

            var toMail = mail.ToMail == null || mail.ToMail.Equals(string.Empty) ? fromMail : mail.ToMail;

            using (var message = new MailMessage(fromMail, toMail))
            {
                message.Subject = mail.Subject;
                message.Body = mail.Body;
                message.IsBodyHtml = true;

                var bcc = mail.CcMail;
                if (bcc != null && !bcc.Equals(string.Empty))
                {
                    message.Bcc.Add(bcc);
                }

                using (var client = new SmtpClient
                {
                    EnableSsl = true,
                    Host = host,
                    Port = port,
                    Credentials = new NetworkCredential(fromMail, password)
                })
                {
                    Logger.Logging.Info($"Trying to send an email from: [{fromMail}] to: [{toMail}] with subject: [{mail.Subject}]");
                    client.Send(message);
                }
            }
        }
    }
}
