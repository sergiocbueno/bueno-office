﻿namespace BuenoOffice.Infrastructure.Utilities.Mail.Interface
{
    public interface ISmtpMail
    {
        void SendMailBySmtpProtocol(Model.Mail mail);
    }
}
