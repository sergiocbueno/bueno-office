﻿namespace BuenoOffice.Infrastructure.Utilities.Mail.Model
{
    public class Mail
    {
        public string CcMail { get; set; }
        public string ToMail { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
