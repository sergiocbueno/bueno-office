CREATE DATABASE buenoofficedb;

CREATE TABLE [dbo].[Faq](
    [Id] [INT] IDENTITY(1,1) NOT NULL,
    [Status] [INT] NOT NULL,
    [Question] [VARCHAR](300) NOT NULL,
    [QuestionDate] [DATETIME] NOT NULL,
	[Answer] [VARCHAR](600) NULL,
    [AnswerDate] [DATETIME] NULL,
 CONSTRAINT [PK_Faq] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
) ON [PRIMARY]

CREATE TABLE [dbo].[User](
	[Id] [INT] IDENTITY(1,1) NOT NULL,
    [Email] [VARCHAR](600) NOT NULL,
    [FirstName] [VARCHAR](100) NOT NULL,
    [LastName] [VARCHAR](300) NOT NULL,
	[Phone] [VARCHAR](15) NOT NULL,
    [Birthday] [DATETIME] NOT NULL,
	[Password] [NVARCHAR](MAX) NOT NULL,
    [IsEmailVerified] [BIT] NOT NULL,
	[ActivationCode] [UNIQUEIDENTIFIER] NOT NULL,
	[HasForgotten] [BIT] NOT NULL,
	[Type] [INT] NOT NULL,
	[Image] [VARBINARY](MAX) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
) ON [PRIMARY]

CREATE TABLE [dbo].[Color](
	[Id] [INT] IDENTITY(1,1) NOT NULL,
    [Name] [VARCHAR](100) NOT NULL,
 CONSTRAINT [PK_Color] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
) ON [PRIMARY]

CREATE TABLE [dbo].[Conversion](
	[Id] [INT] IDENTITY(1,1) NOT NULL,
    [Name] [VARCHAR](100) NOT NULL,
 CONSTRAINT [PK_Conversion] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
) ON [PRIMARY]

CREATE TABLE [dbo].[Brand](
	[Id] [INT] IDENTITY(1,1) NOT NULL,
    [Name] [VARCHAR](100) NOT NULL,
 CONSTRAINT [PK_Brand] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
) ON [PRIMARY]

CREATE TABLE [dbo].[Model](
	[Id] [INT] IDENTITY(1,1) NOT NULL,
    [Name] [VARCHAR](100) NOT NULL,
	[Brand] [INT] NOT NULL,
 CONSTRAINT FK_Brand_Model FOREIGN KEY (Brand) REFERENCES Brand(Id),
 CONSTRAINT [PK_Model] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
) ON [PRIMARY]

CREATE TABLE [dbo].[Version](
	[Id] [INT] IDENTITY(1,1) NOT NULL,
    [Name] [VARCHAR](100) NOT NULL,
	[Model] [INT] NOT NULL,
 CONSTRAINT FK_Model_Version FOREIGN KEY (Model) REFERENCES Model(Id),
 CONSTRAINT [PK_Version] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
) ON [PRIMARY]

CREATE TABLE [dbo].[Car](
	[Id] [INT] IDENTITY(1,1) NOT NULL,
	[Version] [INT] NOT NULL,
	[Conversion] [INT] NOT NULL,
	[Color] [INT] NOT NULL,
	[ModelYear] [INT] NOT NULL,
	[ManufactureYear] [INT] NOT NULL,
	[Mileage] [INT] NULL,
	[CreatedBy] [INT] NOT NULL,
	[CreatedOn] [DATETIME] NOT NULL,
	[UpdatedOn] [DATETIME] NULL,
 CONSTRAINT FK_Version_Car FOREIGN KEY (Version) REFERENCES Version(Id),
 CONSTRAINT FK_Conversion_Car FOREIGN KEY (Conversion) REFERENCES Conversion(Id),
 CONSTRAINT FK_Color_Car FOREIGN KEY (Color) REFERENCES Color(Id),
 CONSTRAINT FK_User_Car FOREIGN KEY (CreatedBy) REFERENCES [User](Id),
 CONSTRAINT [PK_Car] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
) ON [PRIMARY]
