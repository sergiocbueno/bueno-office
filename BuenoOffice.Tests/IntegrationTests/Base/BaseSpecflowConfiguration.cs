﻿using BuenoOffice.DependecyInjection;
using Microsoft.Practices.Unity;
using TechTalk.SpecFlow;

namespace BuenoOffice.Tests.IntegrationTests.Base
{
    public class BaseSpecflowConfiguration
    {
        protected IUnityContainer Container;

        [BeforeScenario]
        public void ScenarioSetup()
        {
            ContainerConfiguration.ConfigureUnityContainer();
            Container = ContainerConfiguration.Container.CreateChildContainer();
        }

        [AfterScenario]
        public void ScenarioCleanup()
        {
        }
    }
}
