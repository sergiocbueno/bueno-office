﻿using System.Linq;
using BuenoOffice.Controllers.Faq;
using BuenoOffice.Data.Entities.Faq.Interface;
using BuenoOffice.Tests.IntegrationTests.Base;
using Microsoft.Practices.Unity;
using TechTalk.SpecFlow;

namespace BuenoOffice.Tests.IntegrationTests.Faq.StepDefinition
{
    [Binding]
    public class FaqSteps : BaseSpecflowConfiguration
    {
        #region Given steps



        #endregion

        #region When steps

        [When("I make a question on FAQ page as '(.*)'")]
        public void WhenIMakeAQuestionOnFaqPage(string question)
        {
            var faqController = new FaqController();
            var actualResult = faqController.SendQuestion(question);
        }

        #endregion

        #region Then steps

        [Then("the result should be (.*) on the screen")]
        public void ThenTheResultShouldBe(Table dataTable)
        {
            
            /*var faqRepository = Container.Resolve<IFaqRepository>();
            faqRepository.Get(faq => faq.Question.Equals(partOfQuestions))*/
        }

        #endregion
    }
}
