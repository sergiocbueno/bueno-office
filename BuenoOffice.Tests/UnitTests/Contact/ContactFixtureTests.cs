﻿using BuenoOffice.Infrastructure.Utilities.Mail.Interface;
using BuenoOffice.Infrastructure.Utilities.Mail.Model;
using BuenoOffice.Service.DTOs.Contact;
using BuenoOffice.Service.Services.Contact.Interface;
using BuenoOffice.Tests.UnitTests.Base;
using Moq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BuenoOffice.Tests.UnitTests.Contact
{
    [TestClass]
    public class ContactFixtureTests : BaseSetupFixture
    {
        [TestMethod, TestCategory("UnitTest")]
        public void Should_invoke_only_one_time_smtp_protocol_on_infrastructure_project()
        {
            var mockSmtpMail = new Mock<ISmtpMail>();
            mockSmtpMail.Setup(x => x.SendMailBySmtpProtocol(It.IsAny<Mail>()));
            Container.RegisterInstance(mockSmtpMail.Object);

            var contactService = Container.Resolve<IContactService>();
            contactService.SendMessage(new ContactMessageDto());

            mockSmtpMail.Verify(x => x.SendMailBySmtpProtocol(It.IsAny<Mail>()), Times.Once);
        }

        [TestMethod, TestCategory("UnitTest")]
        public void Should_return_false_to_email_without_correct_structure()
        {
            var emailWithoutStructure = "test";
            var contactService = Container.Resolve<IContactService>();

            var result = contactService.IsValidEmail(emailWithoutStructure);
            Assert.IsFalse(result);

            emailWithoutStructure = "test+2@gmail.com";
            result = contactService.IsValidEmail(emailWithoutStructure);
            Assert.IsFalse(result);
        }

        [TestMethod, TestCategory("UnitTest")]
        public void Should_return_true_to_email_with_correct_structure()
        {
            var emailWithStructure = "test@test.com";
            var contactService = Container.Resolve<IContactService>();
            var result = contactService.IsValidEmail(emailWithStructure);
            Assert.IsTrue(result);
        }

        [TestMethod, TestCategory("UnitTest")]
        public void Should_return_true_to_phone_with_correct_structure()
        {
            var phoneWithStructure = "(11) 95956-5916";
            var contactService = Container.Resolve<IContactService>();
            var result = contactService.IsValidPhone(phoneWithStructure);
            Assert.IsTrue(result);

            phoneWithStructure = "(11) 3744-5531";
            result = contactService.IsValidPhone(phoneWithStructure);
            Assert.IsTrue(result);
        }

        [TestMethod, TestCategory("UnitTest")]
        public void Should_return_false_to_phone_without_correct_structure()
        {
            var phoneWithoutStructure = "(011) 95956-5916";
            var contactService = Container.Resolve<IContactService>();
            var result = contactService.IsValidPhone(phoneWithoutStructure);
            Assert.IsFalse(result);

            phoneWithoutStructure = "(11) 37445531";
            result = contactService.IsValidPhone(phoneWithoutStructure);
            Assert.IsFalse(result);

            phoneWithoutStructure = "11 3744-5531";
            result = contactService.IsValidPhone(phoneWithoutStructure);
            Assert.IsFalse(result);

            phoneWithoutStructure = "(11) 3744-553";
            result = contactService.IsValidPhone(phoneWithoutStructure);
            Assert.IsFalse(result);

            phoneWithoutStructure = "(11) 744-5531";
            result = contactService.IsValidPhone(phoneWithoutStructure);
            Assert.IsFalse(result);
        }
    }
}
