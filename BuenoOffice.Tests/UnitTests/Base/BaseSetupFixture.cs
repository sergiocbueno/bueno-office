﻿using BuenoOffice.Data.Parametrization.Context;
using BuenoOffice.DependecyInjection;
using Microsoft.Practices.Unity;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BuenoOffice.Tests.UnitTests.Base
{
    public class BaseSetupFixture
    {
        protected IUnityContainer Container;

        [TestInitialize]
        public void SetUpFixture()
        {
            ContainerConfiguration.ConfigureUnityContainer();
            Container = ContainerConfiguration.Container.CreateChildContainer();
        }

        protected Mock<ParametrizationContext> SetMockContext()
        {
            var mockContext = new Mock<ParametrizationContext>();
            Container.RegisterInstance<IParametrizationContext>(mockContext.Object);
            return mockContext;
        }
    }
}
