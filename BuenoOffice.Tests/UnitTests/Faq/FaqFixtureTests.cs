﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BuenoOffice.Data.Entities.Faq.Interface;
using BuenoOffice.Infrastructure.Utilities.Enums.Faq;
using BuenoOffice.Service.DTOs.Faq;
using BuenoOffice.Service.Services.Faq.Interface;
using BuenoOffice.Tests.UnitTests.Base;
using Moq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BuenoOffice.Tests.UnitTests.Faq
{
    [TestClass]
    public class FaqFixtureTests : BaseSetupFixture
    {
        [TestMethod, TestCategory("UnitTest")]
        public void Should_return_last_six_faq_questions_which_have_already_been_answered()
        {
            const string question = "Question";
            const string answer = "Answer";

            var databaseFaqs = new List<Data.Entities.Faq.Model.Faq>
            {
                new Data.Entities.Faq.Model.Faq { Id = 1, Question = question + "1", QuestionDate = new DateTime(2017,01,01), Answer = answer + "1", AnswerDate = new DateTime(2017,01,01), Status = FaqStatus.Answered},
                new Data.Entities.Faq.Model.Faq { Id = 2, Question = question + "2", QuestionDate = new DateTime(2017,01,01), Answer = answer + "2", AnswerDate = new DateTime(2017,01,01), Status = FaqStatus.Answered},
                new Data.Entities.Faq.Model.Faq { Id = 3, Question = question + "3", QuestionDate = new DateTime(2017,01,01), Answer = answer + "3", AnswerDate = new DateTime(2017,01,01), Status = FaqStatus.Answered},
                new Data.Entities.Faq.Model.Faq { Id = 4, Question = question + "4", QuestionDate = new DateTime(2017,01,01), Answer = answer + "4", AnswerDate = new DateTime(2017,01,01), Status = FaqStatus.Answered},
                new Data.Entities.Faq.Model.Faq { Id = 5, Question = question + "5", QuestionDate = new DateTime(2017,01,01), Answer = answer + "5", AnswerDate = new DateTime(2017,01,01), Status = FaqStatus.Answered},
                new Data.Entities.Faq.Model.Faq { Id = 6, Question = question + "6", QuestionDate = new DateTime(2017,01,01), Answer = answer + "6", AnswerDate = new DateTime(2017,01,01), Status = FaqStatus.Answered},
                new Data.Entities.Faq.Model.Faq { Id = 7, Question = question + "7", QuestionDate = new DateTime(2017,01,01), Answer = answer + "7", AnswerDate = new DateTime(2017,01,01), Status = FaqStatus.Answered},
                new Data.Entities.Faq.Model.Faq { Id = 8, Question = question + "8", QuestionDate = new DateTime(2017,01,01), Answer = answer + "8", AnswerDate = new DateTime(2017,01,01), Status = FaqStatus.Answered},
                new Data.Entities.Faq.Model.Faq { Id = 9, Question = question + "9", QuestionDate = new DateTime(2017,01,01), Answer = answer + "9", AnswerDate = new DateTime(2017,01,01), Status = FaqStatus.Answered},
                new Data.Entities.Faq.Model.Faq { Id = 10, Question = question + "10", QuestionDate = new DateTime(2017,01,01), Answer = answer + "10", AnswerDate = new DateTime(2017,01,01), Status = FaqStatus.Answered},
            };

            var expectedResult = new List<FaqDto>
            {
                new FaqDto { Id = 10, Question = question + "10", QuestionDate = new DateTime(2017,01,01), Answer = answer + "10", AnswerDate = new DateTime(2017,01,01), Status = FaqStatus.Answered},
                new FaqDto { Id = 9, Question = question + "9", QuestionDate = new DateTime(2017,01,01), Answer = answer + "9", AnswerDate = new DateTime(2017,01,01), Status = FaqStatus.Answered},
                new FaqDto { Id = 8, Question = question + "8", QuestionDate = new DateTime(2017,01,01), Answer = answer + "8", AnswerDate = new DateTime(2017,01,01), Status = FaqStatus.Answered},
                new FaqDto { Id = 7, Question = question + "7", QuestionDate = new DateTime(2017,01,01), Answer = answer + "7", AnswerDate = new DateTime(2017,01,01), Status = FaqStatus.Answered},
                new FaqDto { Id = 6, Question = question + "6", QuestionDate = new DateTime(2017,01,01), Answer = answer + "6", AnswerDate = new DateTime(2017,01,01), Status = FaqStatus.Answered},
                new FaqDto { Id = 5, Question = question + "5", QuestionDate = new DateTime(2017,01,01), Answer = answer + "5", AnswerDate = new DateTime(2017,01,01), Status = FaqStatus.Answered},
            };

            var faqRepositoryMock = new Mock<IFaqRepository>();
            faqRepositoryMock.Setup(x => x.Get(It.IsAny<Expression<Func<Data.Entities.Faq.Model.Faq, bool>>>())).Returns(databaseFaqs.AsQueryable());
            Container.RegisterInstance(faqRepositoryMock.Object);

            var faqService = Container.Resolve<IFaqService>();
            var actualResult = faqService.LastQuestions();

            faqRepositoryMock.Verify(x => x.Get(It.IsAny<Expression<Func<Data.Entities.Faq.Model.Faq, bool>>>()), Times.Once);

            for (var i = 0; i < actualResult.Count; i++)
            {
                Assert.AreEqual(expectedResult[i].Id, actualResult[i].Id);
                Assert.AreEqual(expectedResult[i].Question, actualResult[i].Question);
                Assert.AreEqual(expectedResult[i].QuestionDate, actualResult[i].QuestionDate);
                Assert.AreEqual(expectedResult[i].Answer, actualResult[i].Answer);
                Assert.AreEqual(expectedResult[i].AnswerDate, actualResult[i].AnswerDate);
                Assert.AreEqual(expectedResult[i].Status, actualResult[i].Status);
            }
            
        }
    }
}
