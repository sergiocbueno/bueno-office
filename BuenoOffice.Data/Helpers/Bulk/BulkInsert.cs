﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace BuenoOffice.Data.Helpers.Bulk
{
    public static class BulkInsert
    {
        public static void SqlBulkInsert(DataTable table)
        {
            const string connectionString = "BuenoOfficeContext";

            using (var dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionString].ConnectionString))
            {
                dbConnection.Open();

                var sqlBulkCopy = new SqlBulkCopy(dbConnection);
                sqlBulkCopy.DestinationTableName = table.TableName;
                sqlBulkCopy.WriteToServer(table);

                dbConnection.Close();
            }
        }
    }
}
