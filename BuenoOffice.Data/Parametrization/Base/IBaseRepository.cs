﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace BuenoOffice.Data.Parametrization.Base
{
    public interface IBaseRepository<TEntity, in TKey> where TEntity : class, IEntity<TKey>
    {
        IQueryable<TEntity> GetAll();
        TEntity Get(TKey id);
        IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> where);

        void Save(TEntity entity);
        void SaveRange(ICollection<TEntity> entities);
        void SaveOrUpdate(TEntity entity);
        void Delete(TKey id);
        void Delete(TEntity entity);
    }
}
