﻿namespace BuenoOffice.Data.Parametrization.Base
{
    public interface IEntity<T>
    {
        T Id { get; }
    }
}
