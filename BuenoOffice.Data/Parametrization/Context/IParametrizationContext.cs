﻿using System;
using System.Data.Entity;

namespace BuenoOffice.Data.Parametrization.Context
{
    public interface IParametrizationContext : IDisposable
    {
        int SaveChanges();
        DbContext DbContext { get; }
    }
}
