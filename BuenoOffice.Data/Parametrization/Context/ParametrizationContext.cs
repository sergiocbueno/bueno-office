﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Text;
using BuenoOffice.Data.Entities.Brand.Mapping;
using BuenoOffice.Data.Entities.Car.Mapping;
using BuenoOffice.Data.Entities.CarModel.Mapping;
using BuenoOffice.Data.Entities.Color.Mapping;
using BuenoOffice.Data.Entities.Conversion.Mapping;
using BuenoOffice.Data.Entities.Faq.Mapping;
using BuenoOffice.Data.Entities.User.Mapping;
using BuenoOffice.Data.Entities.Version.Mapping;
using log4net;
using Microsoft.Practices.Unity;

namespace BuenoOffice.Data.Parametrization.Context
{
    public class ParametrizationContext : DbContext, IParametrizationContext
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ParametrizationContext));
        public virtual DbContext DbContext => this;

        [InjectionConstructor]
        public ParametrizationContext() : base("Name=BuenoOfficeContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Add<OneToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new FaqMap());
            modelBuilder.Configurations.Add(new BrandMap());
            modelBuilder.Configurations.Add(new ModelMap());
            modelBuilder.Configurations.Add(new VersionMap());
            modelBuilder.Configurations.Add(new ColorMap());
            modelBuilder.Configurations.Add(new ConversionMap());
            modelBuilder.Configurations.Add(new CarMap());

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            int result;

            try
            {
                result = base.SaveChanges();
                Log.Info("Persist Unit of Work...");
            }
            catch (DbEntityValidationException ex)
            {
                var stringBuilder = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    stringBuilder.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        stringBuilder.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        stringBuilder.AppendLine();
                    }
                }

                Log.Error($"Exception : {ex}");
                Log.Error($"{stringBuilder}");

                throw new DbEntityValidationException($"Entity Validation Failed - errors follow: {stringBuilder}", ex);
            }
            catch (Exception ex)
            {
                Log.Error($"{ex}");
                throw;
            }

            return result;
        }
    }
}
