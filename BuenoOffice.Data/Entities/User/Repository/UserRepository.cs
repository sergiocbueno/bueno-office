﻿using System.Data.Entity;
using BuenoOffice.Data.Entities.User.Interface;
using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.User.Repository
{
    public class UserRepository : BaseRepository<Model.User, int>, IUserRepository
    {
        public UserRepository(DbContext context) : base(context)
        {
        }
    }
}
