﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuenoOffice.Data.Entities.User.Mapping
{
    internal class UserMap : EntityTypeConfiguration<Model.User>
    {
        public UserMap()
        {
            HasKey(t => t.Id);

            ToTable("User");
            Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(t => t.Email).IsRequired().HasMaxLength(600).HasColumnName("Email");
            Property(t => t.FirstName).IsRequired().HasMaxLength(100).HasColumnName("FirstName");
            Property(t => t.LastName).IsRequired().HasMaxLength(300).HasColumnName("LastName");
            Property(t => t.Phone).IsRequired().HasMaxLength(15).HasColumnName("Phone");
            Property(t => t.Birthday).IsRequired().HasColumnName("Birthday");
            Property(t => t.Password).IsRequired().HasColumnName("Password");
            Property(t => t.IsEmailVerified).IsRequired().HasColumnName("IsEmailVerified");
            Property(t => t.ActivationCode).IsRequired().HasColumnName("ActivationCode");
            Property(t => t.HasForgotten).IsRequired().HasColumnName("HasForgotten");
            Property(t => t.Type).IsRequired().HasColumnName("Type");
            Property(t => t.Image).HasColumnName("Image");
        }
    }
}
