﻿using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.User.Interface
{
    public interface IUserRepository : IBaseRepository<Model.User, int>
    {
    }
}
