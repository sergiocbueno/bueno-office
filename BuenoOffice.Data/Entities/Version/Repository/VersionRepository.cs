﻿using System.Data.Entity;
using BuenoOffice.Data.Entities.Version.Interface;
using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.Version.Repository
{
    public class VersionRepository : BaseRepository<Model.Version, int>, IVersionRepository
    {
        public VersionRepository(DbContext context) : base(context)
        {
        }
    }
}
