﻿using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.Version.Model
{
    public class Version : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        internal int ModelId { get; set; }
        public virtual CarModel.Model.Model Model { get; set; }
    }
}
