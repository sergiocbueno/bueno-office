﻿using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.Version.Interface
{
    public interface IVersionRepository : IBaseRepository<Model.Version, int>
    {
    }
}
