﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuenoOffice.Data.Entities.Version.Mapping
{
    internal class VersionMap : EntityTypeConfiguration<Model.Version>
    {
        public VersionMap()
        {
            HasKey(t => t.Id);

            ToTable("Version");
            Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(t => t.Name).IsRequired().HasMaxLength(100).HasColumnName("Name");
            Property(t => t.ModelId).IsRequired().HasColumnName("Model");
            HasRequired(t => t.Model).WithMany().HasForeignKey(t => t.ModelId);
        }
    }
}
