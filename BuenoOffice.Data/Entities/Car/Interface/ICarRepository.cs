﻿using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.Car.Interface
{
    public interface ICarRepository : IBaseRepository<Model.Car, int>
    {
    }
}
