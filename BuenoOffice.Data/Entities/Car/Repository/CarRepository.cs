﻿using System.Data.Entity;
using BuenoOffice.Data.Entities.Car.Interface;
using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.Car.Repository
{
    public class CarRepository : BaseRepository<Model.Car, int>, ICarRepository
    {
        public CarRepository(DbContext context) : base(context)
        {
        }
    }
}
