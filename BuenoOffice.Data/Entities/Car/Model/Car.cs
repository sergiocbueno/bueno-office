﻿using System;
using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.Car.Model
{
    public class Car : IEntity<int>
    {
        public int Id { get; set; }
        internal int VersionId { get; set; }
        public virtual Version.Model.Version Version { get; set; }
        internal int ConversionId { get; set; }
        public virtual Conversion.Model.Conversion Conversion { get; set; }
        internal int ColorId { get; set; }
        public virtual Color.Model.Color Color { get; set; }
        public int ModelYear { get; set; }
        public int ManufactureYear { get; set; }
        public int? Mileage { get; set; }
        internal int CreatedById { get; set; }
        public virtual User.Model.User CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
