﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuenoOffice.Data.Entities.Car.Mapping
{
    internal class CarMap : EntityTypeConfiguration<Model.Car>
    {
        public CarMap()
        {
            HasKey(t => t.Id);

            ToTable("Car");
            Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(t => t.VersionId).IsRequired().HasColumnName("Version");
            HasRequired(t => t.Version).WithMany().HasForeignKey(t => t.VersionId);
            Property(t => t.ConversionId).IsRequired().HasColumnName("Conversion");
            HasRequired(t => t.Conversion).WithMany().HasForeignKey(t => t.ConversionId);
            Property(t => t.ColorId).IsRequired().HasColumnName("Color");
            HasRequired(t => t.Color).WithMany().HasForeignKey(t => t.ColorId);
            Property(t => t.ModelYear).IsRequired().HasColumnName("ModelYear");
            Property(t => t.ManufactureYear).IsRequired().HasColumnName("ManufactureYear");
            Property(t => t.Mileage).HasColumnName("Mileage");
            Property(t => t.CreatedById).IsRequired().HasColumnName("CreatedBy");
            HasRequired(t => t.CreatedBy).WithMany().HasForeignKey(t => t.CreatedById);
            Property(t => t.CreatedOn).IsRequired().HasColumnName("CreatedOn");
            Property(t => t.UpdatedOn).HasColumnName("UpdatedOn");
        }
    }
}
