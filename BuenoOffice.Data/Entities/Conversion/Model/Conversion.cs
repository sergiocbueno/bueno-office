﻿using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.Conversion.Model
{
    public class Conversion : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
