﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuenoOffice.Data.Entities.Conversion.Mapping
{
    internal class ConversionMap : EntityTypeConfiguration<Model.Conversion>
    {
        public ConversionMap()
        {
            HasKey(t => t.Id);

            ToTable("Conversion");
            Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(t => t.Name).IsRequired().HasMaxLength(100).HasColumnName("Name");
        }
    }
}
