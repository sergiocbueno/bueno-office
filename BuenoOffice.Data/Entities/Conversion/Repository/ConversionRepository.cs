﻿using System.Data.Entity;
using BuenoOffice.Data.Entities.Conversion.Interface;
using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.Conversion.Repository
{
    public class ConversionRepository : BaseRepository<Model.Conversion, int>, IConversionRepository
    {
        public ConversionRepository(DbContext context) : base(context)
        {
        }
    }
}
