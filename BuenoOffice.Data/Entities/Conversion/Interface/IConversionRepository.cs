﻿using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.Conversion.Interface
{
    public interface IConversionRepository : IBaseRepository<Model.Conversion, int>
    {
    }
}
