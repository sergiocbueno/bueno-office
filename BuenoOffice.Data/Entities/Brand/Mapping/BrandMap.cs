﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuenoOffice.Data.Entities.Brand.Mapping
{
    internal class BrandMap : EntityTypeConfiguration<Model.Brand>
    {
        public BrandMap()
        {
            HasKey(t => t.Id);

            ToTable("Brand");
            Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(t => t.Name).IsRequired().HasMaxLength(100).HasColumnName("Name");
        }
    }
}
