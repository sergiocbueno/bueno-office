﻿using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.Brand.Model
{
    public class Brand : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
