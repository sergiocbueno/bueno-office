﻿using System.Data.Entity;
using BuenoOffice.Data.Entities.Brand.Interface;
using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.Brand.Repository
{
    public class BrandRepository : BaseRepository<Model.Brand, int>, IBrandRepository
    {
        public BrandRepository(DbContext context) : base(context)
        {
        }
    }
}
