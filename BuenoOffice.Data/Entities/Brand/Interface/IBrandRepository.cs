﻿using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.Brand.Interface
{
    public interface IBrandRepository : IBaseRepository<Model.Brand, int>
    {
    }
}
