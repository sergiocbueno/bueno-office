﻿using System.Data.Entity;
using BuenoOffice.Data.Entities.Color.Interface;
using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.Color.Repository
{
    public class ColorRepository : BaseRepository<Model.Color, int>, IColorRepository
    {
        public ColorRepository(DbContext context) : base(context)
        {
        }
    }
}
