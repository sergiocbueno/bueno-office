﻿using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.Color.Interface
{
    public interface IColorRepository : IBaseRepository<Model.Color, int>
    {
    }
}
