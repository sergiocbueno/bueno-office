﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuenoOffice.Data.Entities.Color.Mapping
{
    internal class ColorMap : EntityTypeConfiguration<Model.Color>
    {
        public ColorMap()
        {
            HasKey(t => t.Id);

            ToTable("Color");
            Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(t => t.Name).IsRequired().HasMaxLength(100).HasColumnName("Name");
        }
    }
}
