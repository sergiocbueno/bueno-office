﻿using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.Color.Model
{
    public class Color : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
