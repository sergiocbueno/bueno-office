﻿using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.CarModel.Model
{
    public class Model : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        internal int BrandId { get; set; }
        public virtual Brand.Model.Brand Brand { get; set; }
    }
}
