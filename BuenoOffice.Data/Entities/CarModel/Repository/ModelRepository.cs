﻿using System.Data.Entity;
using BuenoOffice.Data.Entities.CarModel.Interface;
using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.CarModel.Repository
{
    public class ModelRepository : BaseRepository<Model.Model, int>, IModelRepository
    {
        public ModelRepository(DbContext context) : base(context)
        {
        }
    }
}
