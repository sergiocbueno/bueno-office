﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuenoOffice.Data.Entities.CarModel.Mapping
{
    internal class ModelMap : EntityTypeConfiguration<Model.Model>
    {
        public ModelMap()
        {
            HasKey(t => t.Id);

            ToTable("Model");
            Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(t => t.Name).IsRequired().HasMaxLength(100).HasColumnName("Name");
            Property(t => t.BrandId).IsRequired().HasColumnName("Brand");
            HasRequired(t => t.Brand).WithMany().HasForeignKey(t => t.BrandId);
        }
    }
}
