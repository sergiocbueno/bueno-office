﻿using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.CarModel.Interface
{
    public interface IModelRepository : IBaseRepository<Model.Model, int>
    {
    }
}
