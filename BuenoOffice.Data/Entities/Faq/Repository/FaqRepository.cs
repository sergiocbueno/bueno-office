﻿using System.Data.Entity;
using BuenoOffice.Data.Entities.Faq.Interface;
using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.Faq.Repository
{
    public class FaqRepository : BaseRepository<Model.Faq, int>, IFaqRepository
    {
        public FaqRepository(DbContext context) : base(context)
        {
        }
    }
}
