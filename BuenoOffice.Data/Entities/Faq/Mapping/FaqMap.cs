﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuenoOffice.Data.Entities.Faq.Mapping
{
    internal class FaqMap : EntityTypeConfiguration<Model.Faq>
    {
        public FaqMap()
        {
            HasKey(t => t.Id);

            ToTable("Faq");
            Property(t => t.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnName("Id");
            Property(t => t.Status).IsRequired().HasColumnName("Status");
            Property(t => t.Question).IsRequired().HasMaxLength(300).HasColumnName("Question");
            Property(t => t.QuestionDate).IsRequired().HasColumnName("QuestionDate");
            Property(t => t.Answer).HasMaxLength(600).HasColumnName("Answer");
            Property(t => t.AnswerDate).HasColumnName("AnswerDate");
        }
    }
}
