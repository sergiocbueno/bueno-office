﻿using BuenoOffice.Data.Parametrization.Base;

namespace BuenoOffice.Data.Entities.Faq.Interface
{
    public interface IFaqRepository : IBaseRepository<Model.Faq, int>
    {
    }
}
