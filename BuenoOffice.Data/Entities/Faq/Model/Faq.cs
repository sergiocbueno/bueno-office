﻿using System;
using BuenoOffice.Data.Parametrization.Base;
using BuenoOffice.Infrastructure.Utilities.Enums.Faq;

namespace BuenoOffice.Data.Entities.Faq.Model
{
    public class Faq : IEntity<int>
    {
        public int Id { get; set; }
        public FaqStatus Status { get; set; }
        public string Question { get; set; }
        public DateTime? QuestionDate { get; set; }
        public string Answer { get; set; }
        public DateTime? AnswerDate { get; set; }
    }
}
